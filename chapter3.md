<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_064{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_064{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:21.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:21.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:19.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:19.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:19.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:19.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(83,222,162);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:32.0px;color:rgb(83,222,162);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:17.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:17.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:30.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:30.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:15.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:15.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:28.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:28.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:26.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:26.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_039{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:21.1px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:21.1px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:21.1px;color:rgb(0,175,80);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:21.1px;color:rgb(0,175,80);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_043{font-family:Arial,serif;font-size:21.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_043{font-family:Arial,serif;font-size:21.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:13.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:13.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_045{font-family:Arial,serif;font-size:14.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_045{font-family:Arial,serif;font-size:14.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_046{font-family:Arial,serif;font-size:20.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_046{font-family:Arial,serif;font-size:20.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_047{font-family:Arial,serif;font-size:9.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_047{font-family:Arial,serif;font-size:9.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_048{font-family:Arial,serif;font-size:16.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_048{font-family:Arial,serif;font-size:16.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_049{font-family:Arial,serif;font-size:40.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_049{font-family:Arial,serif;font-size:40.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_050{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_050{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_051{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_051{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_052{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_052{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_053{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_053{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_054{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_054{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_055{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_055{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_056{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_056{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_057{font-family:Arial,serif;font-size:9.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_057{font-family:Arial,serif;font-size:9.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_058{font-family:Arial,serif;font-size:18.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_058{font-family:Arial,serif;font-size:18.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_059{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_059{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_060{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_060{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_061{font-family:"Calibri Light",serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_061{font-family:"Calibri Light",serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_062{font-family:Arial,serif;font-size:9.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_062{font-family:Arial,serif;font-size:9.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_063{font-family:Arial,serif;font-size:16.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_063{font-family:Arial,serif;font-size:16.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter3/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:258.33px;top:250.48px" class="cls_003"><span class="cls_003">CHAPTER 3</span></div>
<div style="position:absolute;left:152.01px;top:343.68px" class="cls_004"><span class="cls_004">Data Modeling Using the</span></div>
<div style="position:absolute;left:116.51px;top:386.64px" class="cls_004"><span class="cls_004">Entity-Relationship (ER) Model</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Chapter Outline</span></div>
<div style="position:absolute;left:26.08px;top:125.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Overview of Database Design Process</span></div>
<div style="position:absolute;left:26.08px;top:157.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Example Database Application (COMPANY)</span></div>
<div style="position:absolute;left:26.08px;top:189.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  ER Model Concepts</span></div>
<div style="position:absolute;left:62.08px;top:220.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Entities and Attributes</span></div>
<div style="position:absolute;left:62.08px;top:249.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Entity Types, Value Sets, and Key Attributes</span></div>
<div style="position:absolute;left:62.08px;top:278.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Relationships and Relationship Types</span></div>
<div style="position:absolute;left:62.08px;top:307.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Weak Entity Types</span></div>
<div style="position:absolute;left:62.08px;top:336.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Roles and Attributes in Relationship Types</span></div>
<div style="position:absolute;left:26.08px;top:366.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  ER Diagrams - Notation</span></div>
<div style="position:absolute;left:26.08px;top:397.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  ER Diagram for COMPANY Schema</span></div>
<div style="position:absolute;left:26.08px;top:429.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Alternative Notations - UML class diagrams, others</span></div>
<div style="position:absolute;left:26.08px;top:461.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Relationships of Higher Degree</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Overview of Database Design Process</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Two main activities:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Database design</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Applications design</span></div>
<div style="position:absolute;left:26.08px;top:243.68px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Focus in this chapter on </span><span class="cls_064">conceptual database</span></div>
<div style="position:absolute;left:53.08px;top:277.52px" class="cls_064"><span class="cls_064">design</span></div>
<div style="position:absolute;left:62.08px;top:317.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  To design the conceptual schema for a database</span></div>
<div style="position:absolute;left:84.58px;top:348.64px" class="cls_015"><span class="cls_015">application</span></div>
<div style="position:absolute;left:26.08px;top:386.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Applications design focuses on the programs and</span></div>
<div style="position:absolute;left:53.08px;top:420.56px" class="cls_013"><span class="cls_013">interfaces that access the database</span></div>
<div style="position:absolute;left:62.08px;top:459.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Generally considered part of software engineering</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Overview of Database Design Process</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Methodologies for Conceptual</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Design</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Entity Relationship (ER) Diagrams (This Chapter)</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Enhanced Entity Relationship (EER) Diagrams</span></div>
<div style="position:absolute;left:53.08px;top:202.64px" class="cls_013"><span class="cls_013">(Chapter 4)</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Use of Design Tools in industry for designing and</span></div>
<div style="position:absolute;left:53.08px;top:276.56px" class="cls_013"><span class="cls_013">documenting large scale designs</span></div>
<div style="position:absolute;left:26.08px;top:316.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  The UML (Unified Modeling Language) Class</span></div>
<div style="position:absolute;left:53.08px;top:350.48px" class="cls_013"><span class="cls_013">Diagrams are popular in industry to document</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_013"><span class="cls_013">conceptual database designs</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example COMPANY Database</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  We need to create a database schema design</span></div>
<div style="position:absolute;left:53.08px;top:155.60px" class="cls_013"><span class="cls_013">based on the following (simplified) </span><span class="cls_016">requirements</span></div>
<div style="position:absolute;left:53.08px;top:185.60px" class="cls_013"><span class="cls_013">of the COMPANY Database:</span></div>
<div style="position:absolute;left:62.08px;top:222.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  The company is organized into DEPARTMENTs.</span></div>
<div style="position:absolute;left:84.58px;top:250.48px" class="cls_015"><span class="cls_015">Each department has a name, number and an</span></div>
<div style="position:absolute;left:84.58px;top:278.56px" class="cls_015"><span class="cls_015">employee who </span><span class="cls_017">manages</span><span class="cls_015"> the department. We keep</span></div>
<div style="position:absolute;left:84.58px;top:306.64px" class="cls_015"><span class="cls_015">track of the start date of the department manager.</span></div>
<div style="position:absolute;left:84.58px;top:334.48px" class="cls_015"><span class="cls_015">A department may have several locations.</span></div>
<div style="position:absolute;left:62.08px;top:369.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Each department </span><span class="cls_017">controls</span><span class="cls_015"> a number of</span></div>
<div style="position:absolute;left:84.58px;top:397.60px" class="cls_015"><span class="cls_015">PROJECTs. Each project has a unique name,</span></div>
<div style="position:absolute;left:84.58px;top:425.68px" class="cls_015"><span class="cls_015">unique number and is located at a single location.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Example COMPANY Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">(Continued)</span></div>
<div style="position:absolute;left:61.20px;top:122.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  The database will store each EMPLOYEE’s social</span></div>
<div style="position:absolute;left:83.70px;top:150.64px" class="cls_015"><span class="cls_015">security number, address, salary, sex, and</span></div>
<div style="position:absolute;left:83.70px;top:178.48px" class="cls_015"><span class="cls_015">birthdate.</span></div>
<div style="position:absolute;left:97.20px;top:212.64px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> Each employee </span><span class="cls_019">works for</span><span class="cls_008"> one department but may</span></div>
<div style="position:absolute;left:115.20px;top:238.56px" class="cls_019"><span class="cls_019">work on</span><span class="cls_008"> several projects.</span></div>
<div style="position:absolute;left:97.20px;top:270.48px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> The DB will keep track of the number of hours per</span></div>
<div style="position:absolute;left:115.20px;top:296.64px" class="cls_008"><span class="cls_008">week that an employee currently </span><span class="cls_019">works on </span><span class="cls_008">each</span></div>
<div style="position:absolute;left:115.20px;top:322.56px" class="cls_008"><span class="cls_008">project.</span></div>
<div style="position:absolute;left:97.20px;top:354.48px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> It is required to keep track of the </span><span class="cls_019">direct supervisor</span><span class="cls_008"> of</span></div>
<div style="position:absolute;left:115.20px;top:380.64px" class="cls_008"><span class="cls_008">each employee.</span></div>
<div style="position:absolute;left:61.20px;top:411.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Each employee may </span><span class="cls_017">have</span><span class="cls_015"> a number of</span></div>
<div style="position:absolute;left:83.70px;top:439.60px" class="cls_015"><span class="cls_015">DEPENDENTs.</span></div>
<div style="position:absolute;left:97.20px;top:474.48px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> For each dependent, the DB keeps a record of name,</span></div>
<div style="position:absolute;left:115.20px;top:500.64px" class="cls_008"><span class="cls_008">sex, birthdate, and relationship to the employee.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">ER Model Concepts</span></div>
<div style="position:absolute;left:26.08px;top:123.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Entities and Attributes</span></div>
<div style="position:absolute;left:62.08px;top:151.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Entity is a basic concept for the ER model. </span><span class="cls_020">Entities are</span></div>
<div style="position:absolute;left:84.58px;top:173.60px" class="cls_020"><span class="cls_020">specific things or objects</span><span class="cls_010"> in the mini-world that are</span></div>
<div style="position:absolute;left:84.58px;top:194.48px" class="cls_010"><span class="cls_010">represented in the database.</span></div>
<div style="position:absolute;left:98.08px;top:220.48px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  For example the EMPLOYEE John Smith, the Research</span></div>
<div style="position:absolute;left:116.07px;top:239.68px" class="cls_022"><span class="cls_022">DEPARTMENT, the ProductX PROJECT</span></div>
<div style="position:absolute;left:62.08px;top:263.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_020">  Attributes are properties </span><span class="cls_010">used to describe an entity.</span></div>
<div style="position:absolute;left:98.08px;top:290.56px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  For example an EMPLOYEE entity may have the attributes</span></div>
<div style="position:absolute;left:116.07px;top:309.52px" class="cls_022"><span class="cls_022">Name, SSN, Address, Sex, BirthDate</span></div>
<div style="position:absolute;left:62.08px;top:333.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  A specific entity will have a value for each of its attributes.</span></div>
<div style="position:absolute;left:98.08px;top:359.68px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  For example a specific employee entity may have Name='John</span></div>
<div style="position:absolute;left:116.07px;top:379.60px" class="cls_022"><span class="cls_022">Smith', SSN='123456789', Address ='731, Fondren, Houston,</span></div>
<div style="position:absolute;left:116.07px;top:398.56px" class="cls_022"><span class="cls_022">TX', Sex='M', BirthDate='09-JAN-55‘</span></div>
<div style="position:absolute;left:62.08px;top:422.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Each attribute has a </span><span class="cls_023">value set</span><span class="cls_010"> (or data type) associated with</span></div>
<div style="position:absolute;left:84.58px;top:443.60px" class="cls_010"><span class="cls_010">it - e.g. integer, string, date, enumerated type, …</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Types of Attributes (1)</span></div>
<div style="position:absolute;left:26.08px;top:123.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Simple</span></div>
<div style="position:absolute;left:62.08px;top:151.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Each entity has a single atomic value for the attribute. For</span></div>
<div style="position:absolute;left:84.58px;top:172.68px" class="cls_024"><span class="cls_024">example, SSN or Sex.</span></div>
<div style="position:absolute;left:26.08px;top:197.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Composite</span></div>
<div style="position:absolute;left:62.08px;top:226.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  The attribute may be composed of several components. For</span></div>
<div style="position:absolute;left:84.58px;top:246.60px" class="cls_024"><span class="cls_024">example:</span></div>
<div style="position:absolute;left:98.08px;top:271.64px" class="cls_021"><span class="cls_021">n</span><span class="cls_025">  Address(Apt#, House#, Street, City, State, ZipCode, Country), or</span></div>
<div style="position:absolute;left:98.08px;top:294.68px" class="cls_021"><span class="cls_021">n</span><span class="cls_025">  Name(FirstName, MiddleName, LastName).</span></div>
<div style="position:absolute;left:98.08px;top:317.68px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  Composition may form a hierarchy where some components</span></div>
<div style="position:absolute;left:116.07px;top:336.64px" class="cls_022"><span class="cls_022">are themselves composite.</span></div>
<div style="position:absolute;left:26.08px;top:360.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Multi-valued</span></div>
<div style="position:absolute;left:62.08px;top:389.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  An entity may have multiple values for that attribute. For</span></div>
<div style="position:absolute;left:84.58px;top:409.56px" class="cls_024"><span class="cls_024">example, Color of a CAR or PreviousDegrees of a STUDENT.</span></div>
<div style="position:absolute;left:98.08px;top:434.56px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  Denoted as {Color} or {PreviousDegrees}.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Types of Attributes (2)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In general, composite and multi-valued attributes</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">may be nested arbitrarily to any number of levels,</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_013"><span class="cls_013">although this is rare.</span></div>
<div style="position:absolute;left:62.08px;top:235.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  For example, PreviousDegrees of a STUDENT is a</span></div>
<div style="position:absolute;left:84.58px;top:267.52px" class="cls_015"><span class="cls_015">composite multi-valued attribute denoted by</span></div>
<div style="position:absolute;left:84.58px;top:298.48px" class="cls_015"><span class="cls_015">{PreviousDegrees (College, Year, Degree, Field)}</span></div>
<div style="position:absolute;left:62.08px;top:335.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Multiple PreviousDegrees values can exist</span></div>
<div style="position:absolute;left:62.08px;top:373.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Each has four subcomponent attributes:</span></div>
<div style="position:absolute;left:98.08px;top:410.64px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> College, Year, Degree, Field</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:620.47px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example of a composite attribute</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Entity Types and Key Attributes (1)</span></div>
<div style="position:absolute;left:26.08px;top:128.56px" class="cls_026"><span class="cls_026">n</span><span class="cls_027"> Entities with the same basic attributes are</span></div>
<div style="position:absolute;left:53.08px;top:167.68px" class="cls_027"><span class="cls_027">grouped or typed into an </span><span class="cls_028">entity type</span><span class="cls_027">.</span></div>
<div style="position:absolute;left:62.08px;top:212.64px" class="cls_029"><span class="cls_029">n</span><span class="cls_030"> For example, the entity type EMPLOYEE</span></div>
<div style="position:absolute;left:84.58px;top:248.64px" class="cls_030"><span class="cls_030">and PROJECT.</span></div>
<div style="position:absolute;left:26.08px;top:292.48px" class="cls_026"><span class="cls_026">n</span><span class="cls_027"> An attribute of an entity type for which each</span></div>
<div style="position:absolute;left:53.08px;top:330.64px" class="cls_027"><span class="cls_027">entity must have a unique value is called a</span></div>
<div style="position:absolute;left:53.08px;top:369.52px" class="cls_028"><span class="cls_028">key attribute </span><span class="cls_027">of the entity type.</span></div>
<div style="position:absolute;left:62.08px;top:414.48px" class="cls_029"><span class="cls_029">n</span><span class="cls_030"> For example, SSN of EMPLOYEE.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Entity Types and Key Attributes (2)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  A key attribute may be composite.</span></div>
<div style="position:absolute;left:62.08px;top:169.52px" class="cls_031"><span class="cls_031">n</span><span class="cls_032"> VehicleTagNumber is a key of the CAR entity</span></div>
<div style="position:absolute;left:84.58px;top:202.64px" class="cls_032"><span class="cls_032">type with components (Number, State).</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  An entity type may have more than one key.</span></div>
<div style="position:absolute;left:62.08px;top:283.52px" class="cls_031"><span class="cls_031">n</span><span class="cls_032"> The CAR entity type may have two keys:</span></div>
<div style="position:absolute;left:98.08px;top:322.56px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> VehicleIdentificationNumber (popularly called VIN)</span></div>
<div style="position:absolute;left:98.08px;top:357.60px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> VehicleTagNumber (Number, State), aka license</span></div>
<div style="position:absolute;left:116.07px;top:386.64px" class="cls_008"><span class="cls_008">plate number.</span></div>
<div style="position:absolute;left:26.08px;top:421.52px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  </span><span class="cls_064">Each key </span><span class="cls_013">is </span><span class="cls_064">underlined </span><span class="cls_013">(Note: this is different from</span></div>
<div style="position:absolute;left:53.08px;top:455.60px" class="cls_013"><span class="cls_013">the relational schema where only one “primary</span></div>
<div style="position:absolute;left:53.08px;top:488.48px" class="cls_013"><span class="cls_013">key is underlined).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Entity Set</span></div>
<div style="position:absolute;left:7.20px;top:116.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Each entity type will have a collection of entities</span></div>
<div style="position:absolute;left:34.20px;top:150.56px" class="cls_013"><span class="cls_013">stored in the database</span></div>
<div style="position:absolute;left:43.20px;top:190.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Called the </span><span class="cls_033">entity set </span><span class="cls_015">or sometimes </span><span class="cls_033">entity collection</span></div>
<div style="position:absolute;left:7.20px;top:228.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Figure 3.7 shows three CAR entity instances in the</span></div>
<div style="position:absolute;left:34.20px;top:261.68px" class="cls_013"><span class="cls_013">entity set for CAR</span></div>
<div style="position:absolute;left:7.20px;top:302.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Same name (CAR) used to refer to both the </span><span class="cls_034">entity</span></div>
<div style="position:absolute;left:34.20px;top:335.60px" class="cls_034"><span class="cls_034">type</span><span class="cls_013"> and the </span><span class="cls_034">entity set</span></div>
<div style="position:absolute;left:7.20px;top:375.68px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  However, entity type and entity set may be given</span></div>
<div style="position:absolute;left:34.20px;top:409.52px" class="cls_013"><span class="cls_013">different names</span></div>
<div style="position:absolute;left:7.20px;top:449.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Entity set is the current </span><span class="cls_035">state</span><span class="cls_013"> of the entities of that</span></div>
<div style="position:absolute;left:34.20px;top:483.68px" class="cls_013"><span class="cls_013">type that are stored in the database</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">Entity Type CAR with two keys and a</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_011"><span class="cls_011">corresponding Entity Set</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Value Sets (Domains) of Attributes</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Each simple attribute is associated with a value</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">set</span></div>
<div style="position:absolute;left:62.08px;top:202.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  E.g., </span><span class="cls_036">Lastname</span><span class="cls_015"> has a value which is a character</span></div>
<div style="position:absolute;left:84.58px;top:233.68px" class="cls_015"><span class="cls_015">string of upto 15 characters, say</span></div>
<div style="position:absolute;left:62.08px;top:270.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_036">  Date</span><span class="cls_015"> has a value consisting of MM-DD-YYYY</span></div>
<div style="position:absolute;left:84.58px;top:302.56px" class="cls_015"><span class="cls_015">where each letter is an integer</span></div>
<div style="position:absolute;left:26.08px;top:339.68px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  A </span><span class="cls_016">value set </span><span class="cls_013">specifies the set of values associated</span></div>
<div style="position:absolute;left:53.08px;top:373.52px" class="cls_013"><span class="cls_013">with an attribute</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Attributes and Value Sets</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_034">  Value sets </span><span class="cls_013">are similar to </span><span class="cls_034">data types </span><span class="cls_013">in most</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">programming languages - e.g., integer, character</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_013"><span class="cls_013">(n), real, bit</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Mathematically, an attribute A of entity set E</span></div>
<div style="position:absolute;left:53.08px;top:269.60px" class="cls_013"><span class="cls_013">whose value set is V is defined as a function</span></div>
<div style="position:absolute;left:212.07px;top:310.64px" class="cls_013"><span class="cls_013">A : E -> P(V)</span></div>
<div style="position:absolute;left:26.08px;top:350.48px" class="cls_013"><span class="cls_013">Where P(V) indicates a power set (which means all</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_013"><span class="cls_013">possible subsets) of V. The above definition</span></div>
<div style="position:absolute;left:53.08px;top:417.68px" class="cls_013"><span class="cls_013">covers simple and multivalued attributes.</span></div>
<div style="position:absolute;left:26.08px;top:458.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  We refer to the value of attribute A for entity e as</span></div>
<div style="position:absolute;left:53.08px;top:491.60px" class="cls_013"><span class="cls_013">A(e).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Displaying an Entity type</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In ER diagrams, an entity type is displayed in a</span></div>
<div style="position:absolute;left:53.08px;top:155.60px" class="cls_013"><span class="cls_013">rectangular box</span></div>
<div style="position:absolute;left:26.08px;top:192.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Attributes are displayed in ovals</span></div>
<div style="position:absolute;left:62.08px;top:229.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Each attribute is connected to its entity type</span></div>
<div style="position:absolute;left:62.08px;top:263.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Components of a composite attribute are</span></div>
<div style="position:absolute;left:84.58px;top:291.52px" class="cls_015"><span class="cls_015">connected to the oval representing the composite</span></div>
<div style="position:absolute;left:84.58px;top:319.60px" class="cls_015"><span class="cls_015">attribute</span></div>
<div style="position:absolute;left:62.08px;top:354.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Each key attribute is underlined</span></div>
<div style="position:absolute;left:62.08px;top:388.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Multivalued attributes displayed in double ovals</span></div>
<div style="position:absolute;left:26.08px;top:422.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  See the full ER notation in advance on the next</span></div>
<div style="position:absolute;left:53.08px;top:453.68px" class="cls_013"><span class="cls_013">slide</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">NOTATION for ER diagrams</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">Initial Conceptual Design of Entity Types</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_011"><span class="cls_011">for the </span><span class="cls_037">COMPANY </span><span class="cls_011">Database Schema</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Based on the requirements, we can identify four</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">initial entity types in the COMPANY database:</span></div>
<div style="position:absolute;left:62.08px;top:202.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  DEPARTMENT</span></div>
<div style="position:absolute;left:62.08px;top:239.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  PROJECT</span></div>
<div style="position:absolute;left:62.08px;top:277.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  EMPLOYEE</span></div>
<div style="position:absolute;left:62.08px;top:314.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  DEPENDENT</span></div>
<div style="position:absolute;left:26.08px;top:352.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Their initial conceptual design is shown on the</span></div>
<div style="position:absolute;left:53.08px;top:386.48px" class="cls_013"><span class="cls_013">following slide</span></div>
<div style="position:absolute;left:26.08px;top:426.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  The initial attributes shown are derived from the</span></div>
<div style="position:absolute;left:53.08px;top:459.68px" class="cls_013"><span class="cls_013">requirements description</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:25.44px" class="cls_006"><span class="cls_006">Initial Design of Entity Types:</span></div>
<div style="position:absolute;left:25.20px;top:69.36px" class="cls_038"><span class="cls_038">EMPLOYEE, DEPARTMENT, PROJECT, DEPENDENT</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">Refining the initial design by introducing</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_039"><span class="cls_039">relationships</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  The initial design is typically not complete</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Some aspects in the requirements will be</span></div>
<div style="position:absolute;left:53.08px;top:202.64px" class="cls_013"><span class="cls_013">represented as </span><span class="cls_016">relationships</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  ER model has three main concepts:</span></div>
<div style="position:absolute;left:62.08px;top:282.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Entities (and their entity types and entity sets)</span></div>
<div style="position:absolute;left:62.08px;top:320.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Attributes (simple, composite, multivalued)</span></div>
<div style="position:absolute;left:62.08px;top:357.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Relationships (and their relationship types and</span></div>
<div style="position:absolute;left:84.58px;top:388.48px" class="cls_015"><span class="cls_015">relationship sets)</span></div>
<div style="position:absolute;left:26.08px;top:426.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  We introduce relationship concepts next</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Relationships and Relationship Types (1)</span></div>
<div style="position:absolute;left:26.08px;top:123.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  A </span><span class="cls_040">relationship</span><span class="cls_008"> relates two or more distinct entities with a</span></div>
<div style="position:absolute;left:53.08px;top:146.64px" class="cls_008"><span class="cls_008">specific meaning.</span></div>
<div style="position:absolute;left:62.08px;top:175.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  For example, EMPLOYEE John Smith </span><span class="cls_041">works on</span><span class="cls_024"> the ProductX</span></div>
<div style="position:absolute;left:84.58px;top:195.48px" class="cls_024"><span class="cls_024">PROJECT, or EMPLOYEE Franklin Wong </span><span class="cls_041">manages</span><span class="cls_024"> the</span></div>
<div style="position:absolute;left:84.58px;top:215.64px" class="cls_024"><span class="cls_024">Research DEPARTMENT.</span></div>
<div style="position:absolute;left:26.08px;top:240.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Relationships of the same type are grouped or typed into</span></div>
<div style="position:absolute;left:53.08px;top:263.52px" class="cls_008"><span class="cls_008">a </span><span class="cls_040">relationship type</span><span class="cls_008">.</span></div>
<div style="position:absolute;left:62.08px;top:292.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  For example, the WORKS_ON relationship type in which</span></div>
<div style="position:absolute;left:84.58px;top:312.60px" class="cls_024"><span class="cls_024">EMPLOYEEs and PROJECTs participate, or the MANAGES</span></div>
<div style="position:absolute;left:84.58px;top:332.52px" class="cls_024"><span class="cls_024">relationship type in which EMPLOYEEs and DEPARTMENTs</span></div>
<div style="position:absolute;left:84.58px;top:352.68px" class="cls_024"><span class="cls_024">participate.</span></div>
<div style="position:absolute;left:26.08px;top:377.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  The degree of a relationship type is the number of</span></div>
<div style="position:absolute;left:53.08px;top:401.52px" class="cls_008"><span class="cls_008">participating entity types.</span></div>
<div style="position:absolute;left:62.08px;top:429.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Both MANAGES and WORKS_ON are </span><span class="cls_042">binary</span><span class="cls_043"> relationships.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Relationship type vs. relationship set (1)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Relationship Type:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Is the schema description of a relationship</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Identifies the relationship name and the</span></div>
<div style="position:absolute;left:84.58px;top:237.52px" class="cls_015"><span class="cls_015">participating entity types</span></div>
<div style="position:absolute;left:62.08px;top:274.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Also identifies certain relationship constraints</span></div>
<div style="position:absolute;left:26.08px;top:312.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Relationship Set:</span></div>
<div style="position:absolute;left:62.08px;top:352.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  The current set of relationship instances</span></div>
<div style="position:absolute;left:84.58px;top:383.68px" class="cls_015"><span class="cls_015">represented in the database</span></div>
<div style="position:absolute;left:62.08px;top:421.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  The current </span><span class="cls_017">state</span><span class="cls_015"> of a relationship type</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:19.20px;top:12.08px" class="cls_032"><span class="cls_032">Relationship instances of the WORKS_FOR N:1</span></div>
<div style="position:absolute;left:19.20px;top:46.16px" class="cls_032"><span class="cls_032">relationship between EMPLOYEE and DEPARTMENT</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:30.58px;top:25.04px" class="cls_032"><span class="cls_032">Relationship instances of the M:N  WORKS_ON</span></div>
<div style="position:absolute;left:30.58px;top:58.88px" class="cls_032"><span class="cls_032">relationship between EMPLOYEE and PROJECT</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Relationship type vs. relationship set (2)</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Previous figures displayed the relationship sets</span></div>
<div style="position:absolute;left:26.08px;top:163.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Each instance in the set relates individual participating</span></div>
<div style="position:absolute;left:53.08px;top:192.48px" class="cls_008"><span class="cls_008">entities - one from each participating entity type</span></div>
<div style="position:absolute;left:26.08px;top:226.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In ER diagrams, we represent the </span><span class="cls_019">relationship type </span><span class="cls_008">as</span></div>
<div style="position:absolute;left:53.08px;top:255.60px" class="cls_008"><span class="cls_008">follows:</span></div>
<div style="position:absolute;left:62.08px;top:290.64px" class="cls_044"><span class="cls_044">n</span><span class="cls_038">  Diamond-shaped box is used to display a relationship</span></div>
<div style="position:absolute;left:84.58px;top:318.48px" class="cls_038"><span class="cls_038">type</span></div>
<div style="position:absolute;left:62.08px;top:353.52px" class="cls_044"><span class="cls_044">n</span><span class="cls_038">  Connected to the participating entity types via straight</span></div>
<div style="position:absolute;left:84.58px;top:382.56px" class="cls_038"><span class="cls_038">lines</span></div>
<div style="position:absolute;left:62.08px;top:416.64px" class="cls_044"><span class="cls_044">n</span><span class="cls_038">  Note that the relationship type is not shown with an</span></div>
<div style="position:absolute;left:84.58px;top:445.68px" class="cls_038"><span class="cls_038">arrow. The name should be typically be readable from</span></div>
<div style="position:absolute;left:84.58px;top:474.48px" class="cls_038"><span class="cls_038">left to right and top to bottom.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">Refining the COMPANY database</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_011"><span class="cls_011">schema by introducing relationships</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  By examining the requirements, six relationship types are</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_008"><span class="cls_008">identified</span></div>
<div style="position:absolute;left:26.08px;top:192.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  All are </span><span class="cls_019">binary</span><span class="cls_008"> relationships( degree 2)</span></div>
<div style="position:absolute;left:26.08px;top:226.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Listed below with their participating entity types:</span></div>
<div style="position:absolute;left:62.08px;top:260.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  WORKS_FOR (between EMPLOYEE, DEPARTMENT)</span></div>
<div style="position:absolute;left:62.08px;top:292.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  MANAGES (also between EMPLOYEE, DEPARTMENT)</span></div>
<div style="position:absolute;left:62.08px;top:324.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  CONTROLS (between DEPARTMENT, PROJECT)</span></div>
<div style="position:absolute;left:62.08px;top:355.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  WORKS_ON (between EMPLOYEE, PROJECT)</span></div>
<div style="position:absolute;left:62.08px;top:387.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  SUPERVISION (between EMPLOYEE (as subordinate),</span></div>
<div style="position:absolute;left:84.58px;top:414.56px" class="cls_010"><span class="cls_010">EMPLOYEE (as supervisor))</span></div>
<div style="position:absolute;left:62.08px;top:445.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  DEPENDENTS_OF (between EMPLOYEE, DEPENDENT)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:56.20px;top:17.68px" class="cls_011"><span class="cls_011">ER DIAGRAM - Relationship Types are:</span></div>
<div style="position:absolute;left:56.20px;top:56.80px" class="cls_045"><span class="cls_045">WORKS_FOR, MANAGES, WORKS_ON, CONTROLS, SUPERVISION, DEPENDENTS_OF</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Discussion on Relationship Types</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In the refined design, some attributes from the initial entity</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_008"><span class="cls_008">types are refined into relationships:</span></div>
<div style="position:absolute;left:62.08px;top:191.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Manager of DEPARTMENT -> MANAGES</span></div>
<div style="position:absolute;left:62.08px;top:223.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Works_on of EMPLOYEE -> WORKS_ON</span></div>
<div style="position:absolute;left:62.08px;top:255.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Department of EMPLOYEE -> WORKS_FOR</span></div>
<div style="position:absolute;left:62.08px;top:286.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  etc</span></div>
<div style="position:absolute;left:26.08px;top:318.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In general, more than one relationship type can exist</span></div>
<div style="position:absolute;left:53.08px;top:347.52px" class="cls_008"><span class="cls_008">between the same participating entity types</span></div>
<div style="position:absolute;left:62.08px;top:381.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_020">  MANAGES</span><span class="cls_010"> and </span><span class="cls_020">WORKS_FOR</span><span class="cls_010"> are distinct relationship</span></div>
<div style="position:absolute;left:84.58px;top:408.56px" class="cls_010"><span class="cls_010">types between EMPLOYEE and DEPARTMENT</span></div>
<div style="position:absolute;left:62.08px;top:439.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Different meanings and different relationship instances.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Constraints on Relationships</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Constraints on Relationship Types</span></div>
<div style="position:absolute;left:62.08px;top:172.56px" class="cls_009"><span class="cls_009">n</span></div>
<div style="position:absolute;left:84.58px;top:162.56px" class="cls_010"><span class="cls_010">(Also known as ratio constraints)</span></div>
<div style="position:absolute;left:62.08px;top:194.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Cardinality Ratio (specifies </span><span class="cls_023">maximum</span><span class="cls_010"> participation)</span></div>
<div style="position:absolute;left:98.08px;top:225.52px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  One-to-one (1:1)</span></div>
<div style="position:absolute;left:98.08px;top:254.56px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  One-to-many (1:N) or Many-to-one (N:1)</span></div>
<div style="position:absolute;left:98.08px;top:283.60px" class="cls_021"><span class="cls_021">n</span><span class="cls_022">  Many-to-many (M:N)</span></div>
<div style="position:absolute;left:62.08px;top:312.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Existence Dependency Constraint (specifies </span><span class="cls_023">minimum</span></div>
<div style="position:absolute;left:84.58px;top:339.68px" class="cls_010"><span class="cls_010">participation) (also called participation constraint)</span></div>
<div style="position:absolute;left:98.08px;top:370.48px" class="cls_021"><span class="cls_021">n</span><span class="cls_046">  Total participation - every employee work for a department</span></div>
<div style="position:absolute;left:134.07px;top:398.48px" class="cls_047"><span class="cls_047">n</span><span class="cls_048">  Every entity in the total set of employee entities must be related to a</span></div>
<div style="position:absolute;left:152.07px;top:417.68px" class="cls_048"><span class="cls_048">department entity via WORKS_FOR</span></div>
<div style="position:absolute;left:98.08px;top:441.52px" class="cls_021"><span class="cls_021">n</span><span class="cls_046">  partial participation</span></div>
<div style="position:absolute;left:134.07px;top:469.52px" class="cls_047"><span class="cls_047">n</span><span class="cls_048">  Some or part of the set of employee entities are related to some</span></div>
<div style="position:absolute;left:152.07px;top:488.48px" class="cls_048"><span class="cls_048">department entity via MANAGES, but not necessarily all</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:19.92px" class="cls_006"><span class="cls_006">Many-to-one (N:1) Relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:30.58px;top:44.24px" class="cls_049"><span class="cls_049">Many-to-many (M:N) Relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Recursive Relationship Type</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  A relationship type between the same participating entity</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_008"><span class="cls_008">type in </span><span class="cls_040">distinct roles</span></div>
<div style="position:absolute;left:26.08px;top:192.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Also called a</span><span class="cls_040"> self-referencing </span><span class="cls_008">relationship type.</span></div>
<div style="position:absolute;left:26.08px;top:226.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Example: the SUPERVISION relationship</span></div>
<div style="position:absolute;left:26.08px;top:261.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  EMPLOYEE participates twice in two distinct roles:</span></div>
<div style="position:absolute;left:62.08px;top:295.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  supervisor (or boss) role</span></div>
<div style="position:absolute;left:62.08px;top:327.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  supervisee (or subordinate) role</span></div>
<div style="position:absolute;left:26.08px;top:359.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Each relationship instance relates two distinct</span></div>
<div style="position:absolute;left:53.08px;top:388.56px" class="cls_008"><span class="cls_008">EMPLOYEE entities:</span></div>
<div style="position:absolute;left:62.08px;top:422.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  One employee in </span><span class="cls_023">supervisor</span><span class="cls_010"> role</span></div>
<div style="position:absolute;left:62.08px;top:453.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  One employee in </span><span class="cls_023">supervisee</span><span class="cls_010"> role</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:1.28px" class="cls_049"><span class="cls_049">Displaying a recursive</span></div>
<div style="position:absolute;left:25.20px;top:49.28px" class="cls_049"><span class="cls_049">relationship</span></div>
<div style="position:absolute;left:26.08px;top:122.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In a recursive relationship type.</span></div>
<div style="position:absolute;left:62.08px;top:155.60px" class="cls_031"><span class="cls_031">n</span><span class="cls_032"> Both participations are same entity type in</span></div>
<div style="position:absolute;left:84.58px;top:182.48px" class="cls_032"><span class="cls_032">different roles.</span></div>
<div style="position:absolute;left:62.08px;top:216.56px" class="cls_031"><span class="cls_031">n</span><span class="cls_032"> For example, SUPERVISION relationships</span></div>
<div style="position:absolute;left:84.58px;top:242.48px" class="cls_032"><span class="cls_032">between EMPLOYEE (in role of supervisor or</span></div>
<div style="position:absolute;left:84.58px;top:269.60px" class="cls_032"><span class="cls_032">boss) and (another) EMPLOYEE (in role of</span></div>
<div style="position:absolute;left:84.58px;top:296.48px" class="cls_032"><span class="cls_032">subordinate or worker).</span></div>
<div style="position:absolute;left:26.08px;top:330.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In following figure, first role participation labeled</span></div>
<div style="position:absolute;left:53.08px;top:357.68px" class="cls_013"><span class="cls_013">with 1 and second role participation labeled with</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_013"><span class="cls_013">2.</span></div>
<div style="position:absolute;left:26.08px;top:417.68px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In ER diagram, need to display role names to</span></div>
<div style="position:absolute;left:53.08px;top:444.56px" class="cls_013"><span class="cls_013">distinguish participations.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.58px;top:29.04px" class="cls_006"><span class="cls_006">A Recursive Relationship Supervision</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:56.20px;top:13.04px" class="cls_050"><span class="cls_050">Recursive Relationship Type is: </span><span class="cls_051">SUPERVISION</span></div>
<div style="position:absolute;left:56.20px;top:43.04px" class="cls_050"><span class="cls_050">(participation role names are shown)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Weak Entity Types</span></div>
<div style="position:absolute;left:26.08px;top:126.64px" class="cls_018"><span class="cls_018">n</span><span class="cls_022">   An entity that does not have a key attribute and that is identification-</span></div>
<div style="position:absolute;left:53.08px;top:148.48px" class="cls_022"><span class="cls_022">dependent on another entity type.</span></div>
<div style="position:absolute;left:26.08px;top:174.64px" class="cls_018"><span class="cls_018">n</span><span class="cls_022">   A weak entity must participate in an identifying relationship type with</span></div>
<div style="position:absolute;left:53.08px;top:196.48px" class="cls_022"><span class="cls_022">an owner or identifying entity type</span></div>
<div style="position:absolute;left:26.08px;top:222.64px" class="cls_018"><span class="cls_018">n</span><span class="cls_022">   Entities are identified by the combination of:</span></div>
<div style="position:absolute;left:62.08px;top:248.56px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   A partial key of the weak entity type</span></div>
<div style="position:absolute;left:62.08px;top:275.68px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   The particular entity they are related to in the identifying</span></div>
<div style="position:absolute;left:84.58px;top:296.56px" class="cls_037"><span class="cls_037">relationship  type</span></div>
<div style="position:absolute;left:26.08px;top:323.68px" class="cls_018"><span class="cls_018">n</span><span class="cls_053">   Example:</span></div>
<div style="position:absolute;left:62.08px;top:349.60px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   A DEPENDENT entity is identified by the dependent’s first name,</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_054"><span class="cls_054">and</span><span class="cls_037"> the specific EMPLOYEE with whom the dependent is related</span></div>
<div style="position:absolute;left:62.08px;top:397.60px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   Name of DEPENDENT is the </span><span class="cls_054">partial key</span></div>
<div style="position:absolute;left:62.08px;top:424.48px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   DEPENDENT is a </span><span class="cls_054">weak entity type</span></div>
<div style="position:absolute;left:62.08px;top:450.64px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   EMPLOYEE is its identifying entity type via the identifying</span></div>
<div style="position:absolute;left:84.58px;top:472.48px" class="cls_037"><span class="cls_037">relationship type DEPENDENT_OF</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Attributes of Relationship types</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  A relationship type can have attributes:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  For example, HoursPerWeek of WORKS_ON</span></div>
<div style="position:absolute;left:62.08px;top:196.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Its value for each relationship instance describes</span></div>
<div style="position:absolute;left:84.58px;top:224.56px" class="cls_015"><span class="cls_015">the number of hours per week that an EMPLOYEE</span></div>
<div style="position:absolute;left:84.58px;top:252.64px" class="cls_015"><span class="cls_015">works on a PROJECT.</span></div>
<div style="position:absolute;left:98.08px;top:286.56px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> A value of HoursPerWeek depends on a particular</span></div>
<div style="position:absolute;left:116.07px;top:312.48px" class="cls_008"><span class="cls_008">(employee, project) combination</span></div>
<div style="position:absolute;left:62.08px;top:344.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Most relationship attributes are used with M:N</span></div>
<div style="position:absolute;left:84.58px;top:372.64px" class="cls_015"><span class="cls_015">relationships</span></div>
<div style="position:absolute;left:98.08px;top:406.56px" class="cls_018"><span class="cls_018">n</span><span class="cls_008"> In 1:N relationships, they can be transferred to the</span></div>
<div style="position:absolute;left:116.07px;top:432.48px" class="cls_008"><span class="cls_008">entity type on the N-side of the relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:7.20px;top:11.04px" class="cls_006"><span class="cls_006">Example Attribute of a Relationship Type:</span></div>
<div style="position:absolute;left:7.20px;top:54.00px" class="cls_006"><span class="cls_006">Hours of WORKS_ON</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Notation for Constraints on</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Relationships</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Cardinality ratio (of a binary relationship): 1:1,</span></div>
<div style="position:absolute;left:53.08px;top:155.60px" class="cls_013"><span class="cls_013">1:N, N:1, or M:N</span></div>
<div style="position:absolute;left:62.08px;top:192.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Shown by placing appropriate numbers on the</span></div>
<div style="position:absolute;left:84.58px;top:220.48px" class="cls_015"><span class="cls_015">relationship edges.</span></div>
<div style="position:absolute;left:26.08px;top:254.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Participation constraint (on each participating</span></div>
<div style="position:absolute;left:53.08px;top:285.68px" class="cls_013"><span class="cls_013">entity type): </span><span class="cls_034">total</span><span class="cls_013"> (called existence dependency)</span></div>
<div style="position:absolute;left:53.08px;top:315.68px" class="cls_013"><span class="cls_013">or </span><span class="cls_034">partial</span><span class="cls_013">.</span></div>
<div style="position:absolute;left:62.08px;top:352.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Total shown by double line, partial by single line.</span></div>
<div style="position:absolute;left:26.08px;top:386.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  NOTE: These are easy to specify for Binary</span></div>
<div style="position:absolute;left:53.08px;top:416.48px" class="cls_013"><span class="cls_013">Relationship Types.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Alternative (min, max) notation for</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">relationship structural constraints:</span></div>
<div style="position:absolute;left:26.08px;top:132.48px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:124.48px" class="cls_022"><span class="cls_022">Specified on each participation of an entity type E in a relationship</span></div>
<div style="position:absolute;left:53.08px;top:143.68px" class="cls_022"><span class="cls_022">type R</span></div>
<div style="position:absolute;left:26.08px;top:175.68px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:167.68px" class="cls_022"><span class="cls_022">Specifies that each entity e in E participates in at least </span><span class="cls_055">min</span><span class="cls_022"> and at</span></div>
<div style="position:absolute;left:53.08px;top:186.64px" class="cls_022"><span class="cls_022">most </span><span class="cls_055">max</span><span class="cls_022"> relationship instances in R</span></div>
<div style="position:absolute;left:26.08px;top:218.64px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:210.64px" class="cls_022"><span class="cls_022">Default(no constraint): min=0, max=n (signifying no limit)</span></div>
<div style="position:absolute;left:26.08px;top:242.64px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:234.64px" class="cls_022"><span class="cls_022">Must have min</span><span class="cls_056">£</span><span class="cls_022">max, min</span><span class="cls_056">³</span><span class="cls_022">0, max </span><span class="cls_056">³</span><span class="cls_022">1</span></div>
<div style="position:absolute;left:26.08px;top:266.64px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:258.64px" class="cls_022"><span class="cls_022">Derived from the knowledge of mini-world constraints</span></div>
<div style="position:absolute;left:26.08px;top:290.64px" class="cls_018"><span class="cls_018">n</span></div>
<div style="position:absolute;left:53.08px;top:282.64px" class="cls_022"><span class="cls_022">Examples:</span></div>
<div style="position:absolute;left:62.08px;top:306.64px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   A department has exactly one manager and an employee can</span></div>
<div style="position:absolute;left:84.58px;top:325.60px" class="cls_037"><span class="cls_037">manage at most one department.</span></div>
<div style="position:absolute;left:98.08px;top:349.68px" class="cls_057"><span class="cls_057">n</span><span class="cls_058">  Specify (0,1) for participation of EMPLOYEE in MANAGES</span></div>
<div style="position:absolute;left:98.08px;top:371.52px" class="cls_057"><span class="cls_057">n</span><span class="cls_058">  Specify (1,1) for participation of DEPARTMENT in MANAGES</span></div>
<div style="position:absolute;left:62.08px;top:392.56px" class="cls_052"><span class="cls_052">n</span><span class="cls_037">   An employee can work for exactly one department but a</span></div>
<div style="position:absolute;left:84.58px;top:412.48px" class="cls_037"><span class="cls_037">department can have any number of employees.</span></div>
<div style="position:absolute;left:98.08px;top:436.56px" class="cls_057"><span class="cls_057">n</span><span class="cls_058">  Specify (1,1) for participation of EMPLOYEE in WORKS_FOR</span></div>
<div style="position:absolute;left:98.08px;top:457.68px" class="cls_057"><span class="cls_057">n</span><span class="cls_058">  Specify (1,n) for participation of DEPARTMENT in WORKS_FOR</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">The (min,max) notation for</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">relationship constraints</span></div>
<div style="position:absolute;left:109.20px;top:428.64px" class="cls_059"><span class="cls_059">Read the min,max numbers next to the entity</span></div>
<div style="position:absolute;left:109.20px;top:457.68px" class="cls_059"><span class="cls_059">type and looking </span><span class="cls_060">away from </span><span class="cls_059">the entity type</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:26.95px;top:8.80px" class="cls_011"><span class="cls_011">COMPANY ER Schema Diagram using (min,</span></div>
<div style="position:absolute;left:26.95px;top:47.92px" class="cls_011"><span class="cls_011">max) notation</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 45</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Alternative diagrammatic notation</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  ER diagrams is one popular example for</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">displaying database schemas</span></div>
<div style="position:absolute;left:26.08px;top:202.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Many other notations exist in the literature and in</span></div>
<div style="position:absolute;left:53.08px;top:236.48px" class="cls_013"><span class="cls_013">various database design and modeling tools</span></div>
<div style="position:absolute;left:26.08px;top:276.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Appendix A illustrates some of the alternative</span></div>
<div style="position:absolute;left:53.08px;top:310.64px" class="cls_013"><span class="cls_013">notations that have been used</span></div>
<div style="position:absolute;left:26.08px;top:350.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  UML class diagrams is representative of another</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_013"><span class="cls_013">way of displaying ER concepts that is used in</span></div>
<div style="position:absolute;left:53.08px;top:417.68px" class="cls_013"><span class="cls_013">several commercial design tools</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 46</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background47.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Summary of notation for ER diagrams</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 47</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background48.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">UML class diagrams</span></div>
<div style="position:absolute;left:26.08px;top:123.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Represent classes (similar to entity types) as large</span></div>
<div style="position:absolute;left:53.08px;top:146.64px" class="cls_008"><span class="cls_008">rounded boxes with three sections:</span></div>
<div style="position:absolute;left:62.08px;top:174.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Top section includes entity type (class) name</span></div>
<div style="position:absolute;left:62.08px;top:201.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Second section includes attributes</span></div>
<div style="position:absolute;left:62.08px;top:227.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Third section includes class operations (operations are not</span></div>
<div style="position:absolute;left:84.58px;top:248.48px" class="cls_010"><span class="cls_010">in basic ER model)</span></div>
<div style="position:absolute;left:26.08px;top:275.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Relationships (called associations) represented as lines</span></div>
<div style="position:absolute;left:53.08px;top:298.56px" class="cls_008"><span class="cls_008">connecting the classes</span></div>
<div style="position:absolute;left:62.08px;top:327.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Other UML terminology also differs from ER terminology</span></div>
<div style="position:absolute;left:26.08px;top:353.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Used in database design and object-oriented software</span></div>
<div style="position:absolute;left:53.08px;top:376.56px" class="cls_008"><span class="cls_008">design</span></div>
<div style="position:absolute;left:26.08px;top:405.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  UML has many other types of diagrams for software</span></div>
<div style="position:absolute;left:53.08px;top:428.64px" class="cls_008"><span class="cls_008">design</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 48</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background49.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">UML class diagram for COMPANY</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_011"><span class="cls_011">database schema</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 49</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background50.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Other alternative diagrammatic notations</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 50</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:27500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background51.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Relationships of Higher Degree</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Relationship types of degree 2 are called binary</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Relationship types of degree 3 are called ternary</span></div>
<div style="position:absolute;left:53.08px;top:202.64px" class="cls_013"><span class="cls_013">and of degree n are called n-ary</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  In general, an n-ary relationship is not equivalent</span></div>
<div style="position:absolute;left:53.08px;top:276.56px" class="cls_013"><span class="cls_013">to n binary relationships</span></div>
<div style="position:absolute;left:26.08px;top:316.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Constraints are harder to specify for higher-</span></div>
<div style="position:absolute;left:53.08px;top:350.48px" class="cls_013"><span class="cls_013">degree relationships (n > 2) than for binary</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_013"><span class="cls_013">relationships</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 51</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background52.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Discussion of n-ary relationships (n > 2)</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In general, 3 binary relationships can represent different</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_008"><span class="cls_008">information than a single ternary relationship (see Figure</span></div>
<div style="position:absolute;left:53.08px;top:186.48px" class="cls_008"><span class="cls_008">3.17a and b on next slide)</span></div>
<div style="position:absolute;left:26.08px;top:221.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  If needed, the binary and n-ary relationships can all be</span></div>
<div style="position:absolute;left:53.08px;top:249.60px" class="cls_008"><span class="cls_008">included in the schema design (see Figure 3.17a and b,</span></div>
<div style="position:absolute;left:53.08px;top:278.64px" class="cls_008"><span class="cls_008">where all relationships convey different meanings)</span></div>
<div style="position:absolute;left:26.08px;top:313.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In some cases, a ternary relationship can be represented</span></div>
<div style="position:absolute;left:53.08px;top:341.52px" class="cls_008"><span class="cls_008">as a weak entity if the data model allows a weak entity</span></div>
<div style="position:absolute;left:53.08px;top:370.56px" class="cls_008"><span class="cls_008">type to have multiple identifying relationships (and hence</span></div>
<div style="position:absolute;left:53.08px;top:399.60px" class="cls_008"><span class="cls_008">multiple owner entity types) (see Figure 3.17c)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 52</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background53.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example of a ternary relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 53</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background54.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Discussion of n-ary relationships (n > 2)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  If a particular binary relationship can be derived</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">from a higher-degree relationship at all times,</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_013"><span class="cls_013">then it is redundant</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  For example, the TAUGHT_DURING binary</span></div>
<div style="position:absolute;left:53.08px;top:269.60px" class="cls_013"><span class="cls_013">relationship in Figure 3.18 (see next slide) can be</span></div>
<div style="position:absolute;left:53.08px;top:303.68px" class="cls_013"><span class="cls_013">derived from the ternary relationship OFFERS</span></div>
<div style="position:absolute;left:53.08px;top:337.52px" class="cls_013"><span class="cls_013">(based on the meaning of the relationships)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 54</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background55.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_011"><span class="cls_011">Another example of a ternary relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 55</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:30250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background56.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:20.56px" class="cls_011"><span class="cls_011">Displaying constraints on higher-degree</span></div>
<div style="position:absolute;left:25.20px;top:59.68px" class="cls_011"><span class="cls_011">relationships</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  The (min, max) constraints can be displayed on the edges</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_008"><span class="cls_008">- however, they do not fully describe the constraints</span></div>
<div style="position:absolute;left:26.08px;top:192.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Displaying a 1, M, or N indicates additional constraints</span></div>
<div style="position:absolute;left:62.08px;top:226.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  An M or N indicates no constraint</span></div>
<div style="position:absolute;left:62.08px;top:258.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  A 1 indicates that an entity can participate in at most one</span></div>
<div style="position:absolute;left:84.58px;top:284.48px" class="cls_010"><span class="cls_010">relationship instance </span><span class="cls_023">that has a particular combination of the</span></div>
<div style="position:absolute;left:84.58px;top:310.64px" class="cls_023"><span class="cls_023">other participating entities</span></div>
<div style="position:absolute;left:26.08px;top:342.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In general, both (min, max) and 1, M, or N are needed to</span></div>
<div style="position:absolute;left:53.08px;top:371.52px" class="cls_008"><span class="cls_008">describe fully the constraints</span></div>
<div style="position:absolute;left:26.08px;top:406.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Overall, the constraint specification is difficult and possibly</span></div>
<div style="position:absolute;left:53.08px;top:435.60px" class="cls_008"><span class="cls_008">ambiguous when we consider relationships of a degree</span></div>
<div style="position:absolute;left:53.08px;top:463.68px" class="cls_008"><span class="cls_008">higher than two.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 56</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:30800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background57.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Another Example: A UNIVERSITY</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Database</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  To keep track of the enrollments in classes and</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">student grades, another database is to be</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_013"><span class="cls_013">designed.</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  It keeps track of the COLLEGEs,</span></div>
<div style="position:absolute;left:53.08px;top:269.60px" class="cls_013"><span class="cls_013">DEPARTMENTs within each college, the</span></div>
<div style="position:absolute;left:53.08px;top:303.68px" class="cls_013"><span class="cls_013">COURSEs offered by departments, and</span></div>
<div style="position:absolute;left:53.08px;top:337.52px" class="cls_013"><span class="cls_013">SECTIONs of courses, INSTRUCTORs who</span></div>
<div style="position:absolute;left:53.08px;top:370.64px" class="cls_013"><span class="cls_013">teach the sections etc.</span></div>
<div style="position:absolute;left:26.08px;top:410.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  These entity types and the relationships among</span></div>
<div style="position:absolute;left:53.08px;top:444.56px" class="cls_013"><span class="cls_013">these entity types are shown on the next slide in</span></div>
<div style="position:absolute;left:53.08px;top:478.64px" class="cls_013"><span class="cls_013">Figure 3.20.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 57</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:31350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background58.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:53.28px" class="cls_061"><span class="cls_061">UNIVERSITY database conceptual schema</span></div>
<div style="position:absolute;left:245.70px;top:504.12px" class="cls_062"><span class="cls_062">©2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:594.20px;top:508.96px" class="cls_005"><span class="cls_005">Slide 3- 58</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:31900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background59.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Chapter Summary</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  ER Model Concepts: Entities, attributes,</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_013"><span class="cls_013">relationships</span></div>
<div style="position:absolute;left:26.08px;top:202.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Constraints in the ER model</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Using ER in step-by-step mode conceptual</span></div>
<div style="position:absolute;left:53.08px;top:276.56px" class="cls_013"><span class="cls_013">schema design for the COMPANY database</span></div>
<div style="position:absolute;left:26.08px;top:316.64px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  ER Diagrams - Notation</span></div>
<div style="position:absolute;left:26.08px;top:357.68px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Alternative Notations - UML class diagrams,</span></div>
<div style="position:absolute;left:53.08px;top:390.56px" class="cls_013"><span class="cls_013">others</span></div>
<div style="position:absolute;left:26.08px;top:431.60px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Binary Relationship types and those of higher</span></div>
<div style="position:absolute;left:53.08px;top:464.48px" class="cls_013"><span class="cls_013">degree.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 59</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:32450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background60.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Data Modeling Tools (Additional</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Material )</span></div>
<div style="position:absolute;left:26.08px;top:125.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  A number of popular tools that cover conceptual modeling</span></div>
<div style="position:absolute;left:53.08px;top:151.68px" class="cls_008"><span class="cls_008">and mapping into relational schema design.</span></div>
<div style="position:absolute;left:62.08px;top:183.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Examples: ERWin, S- Designer (Enterprise Application</span></div>
<div style="position:absolute;left:84.58px;top:207.68px" class="cls_010"><span class="cls_010">Suite), ER- Studio,  etc.</span></div>
<div style="position:absolute;left:26.08px;top:236.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  POSITIVES:</span></div>
<div style="position:absolute;left:62.08px;top:267.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Serves as documentation of application requirements, easy</span></div>
<div style="position:absolute;left:84.58px;top:291.68px" class="cls_010"><span class="cls_010">user interface - mostly graphics editor support</span></div>
<div style="position:absolute;left:26.08px;top:320.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  NEGATIVES:</span></div>
<div style="position:absolute;left:62.08px;top:352.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Most tools lack a proper distinct notation for relationships</span></div>
<div style="position:absolute;left:84.58px;top:376.64px" class="cls_010"><span class="cls_010">with relationship attributes</span></div>
<div style="position:absolute;left:62.08px;top:405.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Mostly represent a relational design in a diagrammatic form</span></div>
<div style="position:absolute;left:84.58px;top:428.48px" class="cls_010"><span class="cls_010">rather than a conceptual ER-based design</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 60</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:33000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background61.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:14.56px" class="cls_011"><span class="cls_011">Some of the Automated Database</span></div>
<div style="position:absolute;left:79.20px;top:53.68px" class="cls_011"><span class="cls_011">Design Tools </span><span class="cls_037">(Note: Not all may be on the market now)</span></div>
<div style="position:absolute;left:41.85px;top:117.68px" class="cls_063"><span class="cls_063">COMPANY</span></div>
<div style="position:absolute;left:220.55px;top:117.68px" class="cls_063"><span class="cls_063">TOOL</span></div>
<div style="position:absolute;left:470.00px;top:117.68px" class="cls_063"><span class="cls_063">FUNCTIONALITY</span></div>
<div style="position:absolute;left:25.20px;top:144.08px" class="cls_063"><span class="cls_063">Embarcadero</span></div>
<div style="position:absolute;left:139.07px;top:144.08px" class="cls_063"><span class="cls_063">ER Studio</span></div>
<div style="position:absolute;left:352.70px;top:144.08px" class="cls_063"><span class="cls_063">Database Modeling in ER and IDEF1X</span></div>
<div style="position:absolute;left:25.20px;top:163.04px" class="cls_063"><span class="cls_063">Technologies</span></div>
<div style="position:absolute;left:139.07px;top:173.12px" class="cls_063"><span class="cls_063">DB Artisan</span></div>
<div style="position:absolute;left:352.70px;top:173.12px" class="cls_063"><span class="cls_063">Database administration, space and security</span></div>
<div style="position:absolute;left:352.70px;top:192.32px" class="cls_063"><span class="cls_063">management</span></div>
<div style="position:absolute;left:25.20px;top:218.72px" class="cls_063"><span class="cls_063">Oracle</span></div>
<div style="position:absolute;left:139.07px;top:218.72px" class="cls_063"><span class="cls_063">Developer 2000/Designer 2000</span></div>
<div style="position:absolute;left:352.70px;top:218.72px" class="cls_063"><span class="cls_063">Database modeling, application development</span></div>
<div style="position:absolute;left:25.20px;top:245.12px" class="cls_063"><span class="cls_063">Popkin</span></div>
<div style="position:absolute;left:139.07px;top:245.12px" class="cls_063"><span class="cls_063">System Architect 2001</span></div>
<div style="position:absolute;left:352.70px;top:245.12px" class="cls_063"><span class="cls_063">Data modeling, object modeling, process modeling,</span></div>
<div style="position:absolute;left:25.20px;top:264.32px" class="cls_063"><span class="cls_063">Software</span></div>
<div style="position:absolute;left:352.70px;top:264.32px" class="cls_063"><span class="cls_063">structured analysis/design</span></div>
<div style="position:absolute;left:25.20px;top:290.72px" class="cls_063"><span class="cls_063">Platinum</span></div>
<div style="position:absolute;left:139.07px;top:290.72px" class="cls_063"><span class="cls_063">Enterprise Modeling Suite:</span></div>
<div style="position:absolute;left:352.70px;top:290.72px" class="cls_063"><span class="cls_063">Data, process, and business component modeling</span></div>
<div style="position:absolute;left:25.20px;top:309.92px" class="cls_063"><span class="cls_063">(Computer</span></div>
<div style="position:absolute;left:139.07px;top:309.92px" class="cls_063"><span class="cls_063">Erwin, BPWin, Paradigm Plus</span></div>
<div style="position:absolute;left:25.20px;top:328.88px" class="cls_063"><span class="cls_063">Associates)</span></div>
<div style="position:absolute;left:25.20px;top:355.52px" class="cls_063"><span class="cls_063">Persistence</span></div>
<div style="position:absolute;left:139.07px;top:355.52px" class="cls_063"><span class="cls_063">Pwertier</span></div>
<div style="position:absolute;left:352.70px;top:355.52px" class="cls_063"><span class="cls_063">Mapping from O-O to relational model</span></div>
<div style="position:absolute;left:25.20px;top:374.72px" class="cls_063"><span class="cls_063">Inc.</span></div>
<div style="position:absolute;left:25.20px;top:401.12px" class="cls_063"><span class="cls_063">Rational (IBM)</span></div>
<div style="position:absolute;left:139.07px;top:401.12px" class="cls_063"><span class="cls_063">Rational Rose</span></div>
<div style="position:absolute;left:352.70px;top:401.12px" class="cls_063"><span class="cls_063">UML Modeling & application generation in C++/JAVA</span></div>
<div style="position:absolute;left:25.20px;top:430.40px" class="cls_063"><span class="cls_063">Resolution Ltd.</span></div>
<div style="position:absolute;left:139.07px;top:430.40px" class="cls_063"><span class="cls_063">Xcase</span></div>
<div style="position:absolute;left:352.70px;top:430.40px" class="cls_063"><span class="cls_063">Conceptual modeling up to code maintenance</span></div>
<div style="position:absolute;left:25.20px;top:459.44px" class="cls_063"><span class="cls_063">Sybase</span></div>
<div style="position:absolute;left:139.07px;top:459.44px" class="cls_063"><span class="cls_063">Enterprise Application Suite</span></div>
<div style="position:absolute;left:352.70px;top:459.44px" class="cls_063"><span class="cls_063">Data modeling, business logic modeling</span></div>
<div style="position:absolute;left:25.20px;top:485.84px" class="cls_063"><span class="cls_063">Visio</span></div>
<div style="position:absolute;left:139.07px;top:485.84px" class="cls_063"><span class="cls_063">Visio Enterprise</span></div>
<div style="position:absolute;left:352.70px;top:485.84px" class="cls_063"><span class="cls_063">Data modeling, design/reengineering Visual Basic/C++</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 61</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:33550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background62.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Extended Entity-Relationship (EER)</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Model (in the next chapter)</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  The entity relationship model in its original form</span></div>
<div style="position:absolute;left:53.08px;top:202.64px" class="cls_013"><span class="cls_013">did not support the specialization and</span></div>
<div style="position:absolute;left:53.08px;top:236.48px" class="cls_013"><span class="cls_013">generalization abstractions</span></div>
<div style="position:absolute;left:26.08px;top:276.56px" class="cls_012"><span class="cls_012">n</span><span class="cls_013">  Next chapter illustrates how the ER model can be</span></div>
<div style="position:absolute;left:53.08px;top:310.64px" class="cls_013"><span class="cls_013">extended with</span></div>
<div style="position:absolute;left:62.08px;top:350.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Type-subtype and set-subset relationships</span></div>
<div style="position:absolute;left:62.08px;top:387.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Specialization/Generalization Hierarchies</span></div>
<div style="position:absolute;left:62.08px;top:424.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">  Notation to display them in EER diagrams</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasr and Shamkant B. Navathei</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 3- 62</span></div>
</div>

</body>
</html>
