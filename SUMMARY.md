# Summary

* [Introduction](README.md)
* [chapter 1 Databases and Database Users](chapter1.md)
* [lecture 1](lecture1.md)
* [chapter 2 Database System Concepts and Architecture](chapter2.md)
* [lecture 2](lecture2.md)
* [chapter 3 Data Modeling Using the Entity-Relationship (ER) Model](chapter3.md)
* [lecture 3](lecture3.md)
* [DBMS-Intro](DBMS-Intro.md)
* [Example1-Company-ProblemStatement-v1](Example1-Company-ProblemStatement-v1.md)
* [Example1-Company-ERModel-v1](Example1-Company-ERModel-v1.md)
* [Example1-Company-RelationalMapping-v1](Example1-Company-RelationalMapping-v1.md)
* [Ch-3-Slides](Ch-3-Slides.md)
* [Ch-3-Note](Ch-3-Note.md)
* [Ch15-Slides](Ch15-Slides.md)
* [Ch6-Slides](Ch6-Slides.md)
* [SQL-Queries-Part1](SQL-Queries-Part1.md)
* [SQL-Queries-Part2](SQL-Queries-Part2.md)
* [SQL-Embedded-Part3](SQL-Embedded-Part3.md)  
* [BeerDB-SQL-RA-Practice](BeerDB-SQL-RA-Practice.md)
* [BeerDB-SQL-RA-Practice-Solutions-Final](BeerDB-SQL-RA-Practice-Solutions-Final.md)
* [Chapter4_5-SampleExercise-Solutions](Chapter4_5-SampleExercise-Solutions.md)
* [Chapter6-SampleExercise-Solutions](Chapter6-SampleExercise-Solutions.md)
* [Module3-Intro](Module3-Intro.md)
* [StorageStructures-Overview](StorageStructures-Overview.md)
* [TransactionManangement-Overview](TransactionManangement-Overview.md)
* [QueryOptimization-Overview](QueryOptimization-Overview.md)
* [IMDb Homework](imdb.md)










