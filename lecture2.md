## Quiz      

1. Data Models, Schemas, and Instances 
    - Data abstraction 
        - the **suppression of details** of data organization and storage, and the highlighting of the essential features for an improved understanding of data 
        - different **users** can **perceive** data at their preferred level of detail 
    - A data model 
        - a collection of **concepts** that can be used to **describe the structure** of a database—provides the necessary means to achieve this abstraction 
        - basic **operations** for specifying **retrievals** and **updates** on the database 
            - **insert**, **delete**, **modify**, or **retrieve** any kind of object are often included in the **basic** data model operations 
            - **user-defined** operations 

2. Categories of Data Models 
    - **High-level** or conceptual data models 
        - provide **concepts** that are close to the way many **users perceive** data 
    - **low-level** or physical data models 
        - provide concepts that describe the **details** of how **data** is **stored** on the computer storage media 
    - Between these two extremes is a class of **representational** (or implementation) data models, which provide concepts that may be easily understood by end users but that are not too far removed from the way data is organized in computer storage 
        - **Representational** data models **hide** many **details** of data storage on disk but can be implemented on a computer system directly 
        - **Representational** data models represent data by using record structures and hence are sometimes called **record-based data model** 

3. Conceptual **data models** use **concepts** 
    - entities 
        - An **entity** represents a real-world **object** or **concept**, such as an employee or a project from the miniworld that is described in the database 
    - attributes 
        - An **attribute** represents some **property** of interest that further describes an **entity**, such as the employee’s name or salary 
    - relationships 
        - A **relationship among** two or more entities represents an **association** among the entities 
            - a works-on relationship between an employee and a project 

4. The **data models** 
    - The network and  hierarchical model 
    - relational data model 
    - record-based data model 
        - Representational data models 
    - object data model 
        - A standard for object databases called the ODMG object model has been proposed by the Object Data Management Group (ODMG) 
    - self-describing data models 
        - XML 
        - key-value stores 
        - NOSQL system 

5. The database schema 
    - distinguish between the **description of the database** and the **database itself** 
    - The **description of a database** is called the **database schema**, which is specified during database design and is not expected to change frequently 
    - A **displayed schema** is called a **schema diagram** 
        - aspects are **not** specified **in** the **schema diagram** include **relationships** and **constraints** 
    - We call each **object** in the schema—such as STUDENT or COURSE—a **schema construct** 

6. The data in the database at a particular moment in time is called a **database state** or **snapshot**. It is also called the current set of **occurrences** or **instances** in the database 

7. The distinction between **database schema** and **database state** is very important 
    - When we **define a new database**, we specify its **database schema** only to the DBMS 
    - We get the **initial state** of the database when the **database** is first **populated** or **loaded** with the initial data 
    - **valid state**—that is, a state that satisfies the structure and constraints specified in the schema 
    - The DBMS stores the **descriptions of the schema constructs and constraints**—also called the **meta-data**—in the DBMS catalog so that DBMS software can refer to the schema whenever it needs to 
    - The **schema** is sometimes called the **intension**, and a database **state** is called an **extension** of the schema 

8. The Three-Schema Architecture 
    - The **goal** of the three-schema architecture is to **separate** the **user applications** from the **physical database** 
    - achieve and visualize **characteristics** 
        - (1) use of a catalog to store the database description (schema) so as to make it self-describing 
        - (2) insulation of programs and data (program-data and program-operation independence) 
        - (3) support of multiple user views 
    - **The internal schema** 
        - The **internal level** has an **internal schema**, which describes the **physical storage** structure of the database 
        - The **internal schema** uses a **physical data model** and describes the complete details of **data storage** and **access paths** for the database 
    - The **conceptual schema** 
        - The **conceptual level** has a **conceptual schema**, which describes the structure of the whole database for a community of users 
        - The conceptual schema **hides the details** of physical storage structures and concentrates on describing entities, data types, relationships, user operations, and **constraints** 
            - implementation conceptual schema 
    - The **external schemas** 
        - Each external schema describes the part of the database that a particular user group is **interested** in and hides the rest of the database from that user group 
        - Multiple User Views of the Data 
        - conceptual data model 
    - The **mappings** 
        - The processes of **transforming** requests and results between levels are called **mappings**
        - transform requests between the **conceptual** and **internal** levels 

9. The Data Independence 
    - the capacity to **change** the **schema** at **one level** of a database system **without** having to **change** the schema at the next **higher level** 
    - **Logical data independence** 
        - the capacity to change the **conceptual** schema without having to change **external** schemas or application program 
            - change constraints, or to reduce the database, remaining data should not be affected 
    - **Physical data independence** 
        - the capacity to change the **internal** schema **without** having to **change** the **conceptual** schema 
        - the **external** schemas need **not** be **changed** as well 
    - Data independence occurs because when the schema is **changed** at **some level**, the schema at the next **higher** level remains **unchanged**; only the **mapping** between the two levels is **changed**. Hence, application programs referring to the **higher-level** schema (external schema and view schema) need **not** be **changed** 
        - The application programs is not changed 

10. The Database Languages 
    - In DBMSs where a clear separation is maintained between the conceptual and internal level, the **data definition language (DDL)** is used to specify the **conceptual** schema only 
        - In other DBMSs, the DDL is used to define both **conceptual** and **external** schema 
    - The **storage definition language (SDL)**, is used to specify the **internal** schema 
    - The **mappings** between the two schemas may be specified in **either one** of these languages 
    - The **view definition language (VDL)**, is used to specify **user views** and their **mappings** to the conceptual schema 
    - The DBMS provides a set of operations or a language called the **data manipulation language (DML)** for these purposes 
        - Typical **manipulations** 
            - **retrieval**, **insertion**, **deletion**, and **modification** of the data 
        - There are two main **types** of DML 
            - A **high-level** or nonprocedural **DML** 
                - standalone 
                - embedded in a general-purpose programming language 
                - **High-level DMLs**, such as SQL, can specify and retrieve **many records** in a **single DML** statement; therefore, they are called **set-at-a-time** or **set-oriented** DML 
                - A query in a **high-level** DML often specifies **which data to retrieve rather than how to retrieve it**; therefore, such languages are also called **declarative** 
            - A **low-level** or procedural **DML** 
                - must be embedded in a general-purpose programming language 
                - retrieve and process **each record** from a set of records. Low-level DMLs are also called **record-at-a-time** DMLs because of this property 
    - Whenever DML commands, whether high level or low level, are **embedded** in a general-purpose programming **language**, that language is called the **host languag**e and the **DML** is called the **data sublanguage** 
    - a **high-level** DML used in a **standalone** interactive manner is called a **query** language 

11. The DBMS Interfaces 
    - user-friendly interfaces 
        - Menu-based Interfaces 
        - Forms-based Interfaces 
        - Graphical User Interfaces 
        - Natural Language Interfaces 

12. The **precompiler** extracts DML commands from an application program written in a host programming language 

13. It is also becoming increasingly common to use **scripting languages** such as **PHP** and **Python** to write database programs 

14. The **client program** that accesses the DBMS running on a separate computer is called the **client computer** running DBMS client software 

15. The device from the computer on which the **database resides** is called the **database server** 

16. The client accesses a **middle computer**, called the **application server**, which in turn accesses the database server 

17. The Database System Utilities 
    - The database **utilities** that **help** the DBA **manage** the database system 
    - **Loading** 
        - A **loading** utility is used to load existing **data** files—such as **text** files or **sequential** files—into the database 
    - **Backup** 
        - A **backup** utility creates a backup copy of the database, usually by dumping the entire database onto **tape** or other mass **storage** medium 
        - **Incremental** backup is more **complex**, but **saves** storage space 
    - Database storage **reorganization** 
        - This utility can be used to **reorganize** a set of database files into different **file organizations** and create new access paths to improve performance 
    - **Performance monitoring** 
        - monitors database **usage** and provides **statistics** to the DBA 
        - The **DBA** uses the **statistics** in **making decisions** to **improve performance** 
    - Other utilities 
        - sorting files 
        - handling data compression 
        - monitoring access by users 
        - interfacing with the network 
        - performing other functions 
    - The data dictionary, data repository, information repository 
        - In addition to storing **catalog information** about **schemas** and **constraints**, the data dictionary stores other information, such as **design decisions**, **usage standards**, **application program descriptions**, and **user information** 
            - Active data dictionary is accessed by DBMS software and users/DBA 
            - Passive data dictionary is accessed by users/DBA only 
    - **Application development environments**, such as PowerBuilder (Sybase)  or JBuilder (Borland), have been quite popular 

18. The **Centralized** DBMSs Architecture 
    - The DBMS itself was still a **centralized** DBMS in which **all** the DBMS functionality application program execution, and user interface processing were carried out **on one machine** 

19. The Basic Client/Server Architectures 
    - The specialized servers with specific functionalities 
        - It is possible to connect a number of PCs or small workstations as clients to a **file server** that maintains the files of the client machines. Another machine can be designated as a **printer server** by being connected to various printers; all print requests by the clients are forwarded to this machine. **Web servers** or **e-mail servers** also fall into the specialized server category. The resources provided by specialized servers can be accessed by many client machine 

20. The concept of **client/server** architecture assumes an underlying framework that consists of many PCs/workstations and mobile devices as well as a smaller number of server machines, **connected via wireless networks** or LANs and other types of computer networks 
    - A **client** in this framework is typically a user machine that **provides user interface** capabilities and local processing 
    - A **server** is a system containing both hardware and software that can **provide services** to the client machines, such as file access, printing, archiving, or database access 

21. Two main types of basic DBMS architectures were created on this underlying **client/server framework**: **two-tier** and **three-tier** 
    - **Two-Tier** Client/Server Architectures for DBMSs 
        - two-tier architectures client and server 
    - **Three-Tier** and n-Tier Architectures  for Web Applications 
        - adds an **intermediate** layer between the client and the database server 
        - This **intermediate** layer or middle tier is called the **application server** or the **Web server** 
            - accepts requests from the client 
            - processes the request and sends database queries and commands to the database server 

22. Classification of Database Management Systems 
    - hierarchical model 
    - network data model 
    - relational data model 
        - SQL system 
    - object data model 
    - NOSQL systems 
        - document-based 
            - based on JSON (Java Script Object Notation) and stores the data as documents, which somewhat resemble complex object 
        - graph-based 
            - stores objects as graph nodes and rela- tionships among objects as directed graph edge 
        - column-based 
            - store the columns of rows clustered on disk pages for fast access and allow multiple versions of the data 
        - key-value data model 
            - associates a unique key with each value (which can be a record or object) and provides very fast access to a value given its key 

23. The native **XML** DBMSs is a **hierarchical tree-structured** data model 
    - It combines database concepts with concepts from document **representation models**. Data is represented as elements; with the use of tags, data can be nested to create complex **tree** structure 
    - resembles the **object** model 

24. The criterion used to classify DBMSs is the **number of users** supported by the system 
    - Single-user systems support only one user at a time and are mostly used with PCs 
    - Multiuser systems, which include the majority of DBMSs, support concurrent multiple users 

25. The criterion is the **number of sites** over which the database is distributed 
    - A DBMS is **centralized** if the data is stored at a single computer site 
        - the DBMS and the database **reside** totally at a **single** computer site 
    - A **distributed** DBMS (DDBMS) can have the actual database and DBMS software distributed over many sites connected by a computer network 
        - The data is often **replicated** on multiple sites so that failure of a site will not make some data unavailable 

26. The DDBMS 
    - **Homogeneous** DDBMSs use the **same** DBMS **software** at all the sites 
    - **heterogeneous** DDBMSs can use **different** DBMS **software** at each site 
    - This leads to a **federated** DBMS (or multidatabase system), in which the **participating DBMSs are loosely coupled and have a degree of local autonomy** 
    - Many DDBMSs use **client-server** architecture 

27. The classification of DBMSs based on cost 
    - **open source** (free) DBMS products 
        - MySQL 
        - PostgreSQL 
    - It is possible to pay **millions of dollars** for the installation and maintenance of large database systems annually 

28. The classify a DBMS on the basis of the types of access path options for storing files 
    - based on inverted file structures 
        - indexing 

29. The general purpose or special purpose 
    - a special-purpose DBMS can be designed and built for a specific application 
        - The **online transaction processing (OLTP) systems**, which must support a large number of concurrent transactions without imposing excessive delay 




















        
































    

























































