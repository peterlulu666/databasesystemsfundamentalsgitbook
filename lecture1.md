## Quiz      

1. These interactions are examples of what we may call **traditional database applications**, in which most of the information that is stored and accessed is either **textual or numeric** 

2. These types of files are becoming an important component of **multimedia databases**. **Geographic information systems (GISs)** can store and analyze maps, weather data, and satellite images.** Data warehouses** and **online analytical processing (OLAP)** systems are used in many companies to extract and analyze useful business information from very large databases to support decision making. **Real-time** and **active database technology** is used to control industrial and manufacturing processes. And database **search techniques** are being applied to the World Wide Web to improve the search for information that is needed by users browsing the Internet 

3. A **database** is a **collection of related data**. By **data**, we mean known facts that can be **recorded** and that have implicit **meaning**

4. A **database** has the following implicit **properties** 
    - A database represents some aspect of the real world, sometimes called the **miniworld** or the **universe of discourse (UoD)** 
    - A database is a logically coherent collection of data with some **inherent meaning** 
    - A database is designed, built, and populated with data for a **specific purpose** 

5. **A database management system (DBMS)** is a **computerized system** that enables users to **create** and **maintain** a database 
    - **Defining** a database involves specifying the data types, structures, and constraints of the data to be stored in the database 
    - The **database definition** or **descriptive information** is also **stored** by the DBMS **in the form** of a **database catalog or dictionary**; it is called **meta-data** 
    - **Constructing** the database is the process of **storing** the data on some storage medium that is controlled by the DBMS 
    - **Manipulating** a database includes functions such as **querying** the database to retrieve specific data, **updating** the database to reflect changes in the miniworld, and **generating** reports from the data 
    - **Sharing** a database allows multiple users and programs to **access** the database simultaneously 

6. The important functions provided by the DBMS 
    - A **query** typically causes some data to be **retrieved** 
    - a **transaction** may cause some data to be **read** and some data to be **written** into the database 
    - **Protection** includes system protection against hardware or software malfunction (or crashes) and security protection against unauthorized or malicious access 
    - A typical large database may have a life cycle of many years, so the DBMS must be able to **maintain** the database system by allowing the system to evolve as **requirements change** over time 

7. The design is then translated to a **logical design** that can be **expressed in a data model** implemented in a commercial DBMS 

8. The final stage is **physical design**, during which further specifications are provided for **storing** and **accessing** the database 

9. The Characteristics of the Database Approach 
    - Self-describing nature of a database system 
    - Insulation between programs and data, and data abstraction 
    - Support of multiple views of the data 
    - Sharing of data and multiuser transaction processing 

10. The Self-Describing Nature of a Database System 
    - A fundamental characteristic of the database approach is that the database system contains not only the database itself but also a complete **definition** or **description** of the database structure and constraints 
        - This **definition** is stored in the DBMS catalog, which contains information such as the **structure** of each file, the type and storage format of each data item, and various **constraints** on the data 
        - The **information stored** in the catalog is called **meta-data** 
            - **NOSQL systems**, do **not** require **meta-data**. Rather the data is stored as **self-describing** data that includes the data item names and data values together in one structure 
        - work equally well with **any** number of **database** application 

11. The Insulation between Programs and Data, and Data Abstraction 
    - DBMS access programs do **not** require such require **changing** all programs in most cases. The structure of data files is stored in the DBMS catalog separately from the access programs. We call this property program-data **independence** 
    - User application programs can operate on the data by invoking these opera- tions through their names and arguments, regardless of how the operations are implemented. This may be termed program-operation **independence** 
    - The characteristic that allows program-data independence and program-operation independence is called data **abstraction** 
        - a **data model** is a type of data **abstraction** that is used to provide this **conceptual** representation 
        - the data model **hides** storage and implementation details that are not of interest to most database user 
        - A major part of this text is devoted to **presenting** various data models and the **concepts** they use to abstract the representation of data 

12. The Support of Multiple Views of the Data 
    - A database typically has many types of users, each of whom may require a different perspective or **view** of the database 
        - One user may be **interested** only in accessing and printing the transcript of each student. A second user, who is **interested** only in checking that students have taken all the prerequisites of each course for which the student registers 

13. The Sharing of Data and Multiuser Transaction Processing 
    - A multiuser DBMS, as its name implies, must allow **multiple users** to access the database at the same time 
    - The DBMS must include  concurrency control software to ensure that several users trying to update the same data do so in a controlled manner so that the result of the updates is correct 
    - the DBMS should ensure that each seat can be accessed by **only one** agent at a time for assignment to a passenger. These types of applications are generally called **online transaction processing (OLTP) application** 
    - The DBMS must enforce several transaction **properties** 
        - The **isolation** property ensures that each transaction appears to execute in **isolation** from other transactions, even though hundreds of transactions may be executing concurrently 
        - The **atomicity** property ensures that either **all** the database operations in a transaction are executed **or** **none** are 

14. We categorize the different types of **people** who work in a **database** system environment 
    - Actors on the Scene 
        - people whose jobs involve the day-to-day **use** of a large database 
        - **Database Administrators** 
            - Administering these resources is the responsibility of the **database administrator (DBA)**. The **DBA** is responsible for authorizing access to the database, coordinating and monitoring its use, and acquiring software and hardware resources as needed 
        - **Database Designers** 
            - **Database designers** are responsible for **identifying the data to be stored** in the database and for choosing appropriate structures to represent and store this data 
            - It is the responsibility of **database designers** to **communicate** with all prospective database users in order to understand their requirements and to create a design that meets these requirement 
        - **End Users** 
            - **End users** are the people whose jobs require **access to the database** for querying, updating, and generating report 
            - **Casual** end users **occasionally** access the database, but they may need differ- ent information each time 
                - Casual users learn only **a few facilities** that they may use repeatedly
            - **Naive or parametric** end users make up a **sizable** portion of database end user 
                - canned transactions 
                - mobile app 
                - they simply have to understand the **user interfaces** of the mobile apps or standard transactions designed and implemented for their use 
            - **Sophisticated** end users include engineers, scientists, business analysts, and others who thoroughly familiarize themselves with the facilities of the DBMS in order to **implement** their own **applications** to meet their complex requirement 
                - Sophisticated users try to learn **most of the DBMS facilities** in order to achieve their complex requirement 
            - **Standalone** users **maintain** personal databases by **using** ready-made **program packages** that provide easy-to-use menu-based or graphics-based interfaces. An example is the user of a financial software package that stores a variety of personal financial data 
                - Standalone users typically become very **proficient** in using a specific software package 
        - **System Analysts and Application Programmers  (Software Engineers)** 
            - **System analysts** determine the requirements of end users, especially naive and parametric end users, and develop specifications for standard canned transactions that meet these requirement 
            - **Application programmers** implement these specifi- cations as programs; then they test, debug, document, and maintain these canned transactions. Such analysts and programmers—commonly referred to as **software developers** or **software engineers**—should be familiar with the full range of capabilities provided by the DBMS to accomplish their tasks 
    - Workers behind the Scene 
        - work to **maintain** the database system environment 
        - design, development, and operation of the DBMS **software and system environment** 
        - **DBMS system designers and implementers** design and implement the DBMS modules and interfaces as a software package. The DBMS must interface with other system software, such as the operating system and compilers for various programming language 
        - **Tool developers** design and implement **tools**—the software packages that facilitate database modeling and design, database system design, and improved performance 
        - **Operators and maintenance personnel** (system administration personnel) are responsible for the actual running and **maintenance** of the hardware and software environment for the database system 

15. **Advantages** of Using the **DBMS** Approach 
    - **Controlling Redundancy** 
        - **duplication of effort** 
        - **storage** space is **wasted** 
        - files that represent the same data may become **inconsistent** 
        - **data normalization** ensures consistency and saves storage space 
        - **data denormalization** 
            - it is sometimes necessary to use **controlled redundancy** to improve the performance of queries 
    - **Restricting Unauthorized Access** 
        - A DBMS should provide a security and authorization subsystem 
        - only the DBA’s staff may be allowed to use certain **privileged software**, such as the software for creating new account 
    - **Providing Persistent Storage for Program Objects** 
        - Databases can be used to provide **persistent** storage for program objects and data structure 
        - a complex object in C++ can be stored permanently in an object-oriented DBMS. Such an object is said to be persistent 
    - **Providing Storage Structures and Search Techniques for Efficient Query Processing** 
        - Auxiliary files called **indexes** 
            - specialized data structures and search techniques to speed up disk search for the desired record 
            - **Indexes** are typically based on **tree** data structures or **hash** data structures that are suitably modified for disk search 
        - **buffering** or **caching** module that maintains parts of the database in main memory buffers 
            - In order to process the database records needed by a particular query, those records must be **copied** from disk to main memory 
    - **The query processing and optimization module of the DBMS** is responsible for **choosing** an efficient query execution plan for each query based on the existing **storage** structure 
    - **Providing Backup and Recovery** 
        - A DBMS must provide facilities for **recovering** from hardware or software failures. The **backup and recovery subsystem** of the DBMS is responsible for **recovery** 
    - **Providing Multiple User Interfaces** 
        - Because many types of users with **varying levels of technical knowledge** use a database, a DBMS should provide a variety of user interface 
        - Both **forms-style interfaces** and **menu-driven interfaces** are commonly known as graphical user interfaces (GUIs) 
            - Web GUI interfaces to a database—or Web-enabling a database—are also quite common 
    - **Representing Complex Relationships among Data** 
        - A DBMS must have the capability to represent a variety of complex **relationships** among the data, to define new relationships as they arise, and to retrieve and update related data easily and efficiently 
    - **Enforcing Integrity Constraints** 
        - we can specify that every section record must be **related** to a course record. This is known as a **referential integrity constraint** 
        - Another type of constraint specifies **uniqueness** on data item values, such as every course record must have a unique value for Course_number. This is known as a **key or uniqueness constraint** 
        - **Rules** that pertain to a specific data model are called **inherent rules** of the data model 
    - **Permitting Inferencing and Actions  Using Rules and Triggers** 
        - Some database systems provide capabilities for defining deduction rules for **inferencing** new information from the stored database facts. Such systems are called **deductive database system** 
        - A **trigger** is a form of a rule activated by updates to the table, which results in performing some additional oper- ations to some other tables, sending messages, and so on 

16. Additional **Implications** of Using the Database Approach 
    - **Potential for Enforcing Standards** 
        - The database approach permits the DBA to define and enforce standards among database users in a large organization 
        - Standards can be defined for names and formats of data elements, display formats, report structures, terminology, and so on 
    - **Reduced Application Development Time** 
        - **less time** is generally required to create new applications 
    - **Flexibility** 
        - It may be necessary to **change** the **structure** of a **database** as requirements change. Modern DBMSs allow certain types of evolutionary changes to the structure of the database **without affecting** the **stored data** and the existing application programs 
    - **Availability of Up-to-Date Information** 
        - A DBMS makes the database available to **all users**. As soon as **one** user’s **update** is **applied to** the database, **all** other users can immediately see this **update** 
    - **Economies of Scale** 
        - The DBMS approach permits consolidation of data and applications, thus reducing the amount of **wasteful overlap** between activities of data-processing personnel in different projects or departments as well as redundancies among application 
        - This **reduces** overall **costs** of operation and management 

17. A Brief **History** of Database Applications 
    - Early Database Applications Using **Hierarchical** and Network Systems 
        - One of the main problems with early database systems was the **intermixing** of conceptual relationships with the physical storage and placement of records on disk 
        - Another shortcoming of early systems was that they provided **only** programming language interfaces 
        - The main types of early systems were based on three main paradigms: **hierarchical** systems, **network** model–based systems, and **inverted** file system 
    - Providing Data Abstraction and Application Flexibility  with **Relational** Databases 
        - **high-level** query languages 
        - **alternative** to programming language interfaces 
        - **faster** to write new queries 
        - **data abstraction** and **program-data independence** were much improved 
        - With the development of **new storage and indexing techniques** and **better query processing and optimization**, their performance improved 
    - **Object-Oriented** Applications and the Need for More Complex Databases 
        - The emergence of **object-oriented programming languages** in the 1980s and the need to **store and share complex**, structured objects led to the development of object-oriented databases (OODBs) 
        - many **object-oriented** concepts were incorporated into the newer versions of relational DBMSs, leading to object-relational database management systems, known as ORDBMSs 
    - Interchanging Data on the **Web** for **E-Commerce** Using XML 
        - **Hyper- Text Markup Language (HTML)**, and store these documents on Web servers where other users (clients) can access them and view them through Web browsers 
        - The **eXtended Markup Language (XML)** is one standard for interchanging data among various types of **databases** and Web pages 
        - A variety of **techniques** were developed to allow the interchange of **dynamically** extracted data on the Web for display on Web pages 
            - PHP 
            - JavaScript 

18. Extending Database Capabilities  for New Applications 
    - **Scientific applications** that store large amounts of data  
        - high-energy physics 
        - the mapping of the human genome 
        - the discovery of protein structure 
    - Storage and retrieval of **images** 
        - scanned news or personal **photographs** 
        - satellite photographic **images** 
        - **images** from medical procedures such as x-rays and MRI 
    - Storage and retrieval of **videos** 
        - **movies** 
        - **video** clips from news or personal digital cameras 
    - **Data mining** applications that analyze large amounts of data 
        - search for the occurrences of specific **patterns** or **relationships** 
    - **Spatial applications** that store and analyze spatial locations of data 
        - weather information 
        - maps used in geographical information systems 
        - automobile navigational system 
    - **Time** series 
        - economic data at regular points in time 
        - daily sales 
        - monthly gross national product figure 
    - Emergence of **Big Data** Storage Systems and **NOSQL** Databases 
        - Hadoop  
        - NOSQL 
            - The term NOSQL is generally interpreted as Not Only SQL, meaning that in systems than manage large amounts of data, some of the data is stored using SQL systems, whereas other data would be stored using NOSQL, depending on the application requirement 

19. When Not to Use a DBMS 
    - The overhead costs 
        - High initial investment in hardware, software, and training 
        - The generality that a DBMS provides for defining and processing data 
        - Overhead for providing security, concurrency control, recovery, and integ- rity function 
    - Simple, well-defined database applications that are not expected to change at all 
    - Stringent, real-time requirements 
    - Embedded systems with limited storage capacity 
    - No multiple-user access to data 


















































































































    








