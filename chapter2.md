<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:26.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:26.0px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:26.0px;color:rgb(255,191,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:26.0px;color:rgb(255,191,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter2/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:258.33px;top:250.48px" class="cls_003"><span class="cls_003">CHAPTER 2</span></div>
<div style="position:absolute;left:126.01px;top:343.68px" class="cls_004"><span class="cls_004">Database System Concepts</span></div>
<div style="position:absolute;left:231.51px;top:386.64px" class="cls_004"><span class="cls_004">and Architecture</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Outline</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Models and Their Categories</span></div>
<div style="position:absolute;left:26.08px;top:162.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:199.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Schemas, Instances, and States</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Three-Schema Architecture</span></div>
<div style="position:absolute;left:26.08px;top:273.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Independence</span></div>
<div style="position:absolute;left:26.08px;top:310.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  DBMS Languages and Interfaces</span></div>
<div style="position:absolute;left:26.08px;top:347.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database System Utilities and Tools</span></div>
<div style="position:absolute;left:26.08px;top:384.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Centralized and Client-Server Architectures</span></div>
<div style="position:absolute;left:26.08px;top:421.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Classification of DBMSs</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Data Models</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Data Model:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  A set of concepts to describe the </span><span class="cls_013">structure</span><span class="cls_012"> of a database,</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">the </span><span class="cls_013">operations </span><span class="cls_012">for manipulating these structures, and</span></div>
<div style="position:absolute;left:84.58px;top:215.60px" class="cls_012"><span class="cls_012">certain </span><span class="cls_013">constraints</span><span class="cls_012"> that the database should obey.</span></div>
<div style="position:absolute;left:26.08px;top:247.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Data Model Structure and Constraints:</span></div>
<div style="position:absolute;left:62.08px;top:282.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Constructs are used to define the database structure</span></div>
<div style="position:absolute;left:62.08px;top:313.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Constructs typically include </span><span class="cls_013">elements </span><span class="cls_012">(and their </span><span class="cls_013">data</span></div>
<div style="position:absolute;left:84.58px;top:340.64px" class="cls_013"><span class="cls_013">types</span><span class="cls_012">) as well as groups of elements (e.g. </span><span class="cls_013">entity, record,</span></div>
<div style="position:absolute;left:84.58px;top:366.56px" class="cls_013"><span class="cls_013">table</span><span class="cls_012">), and </span><span class="cls_013">relationships</span><span class="cls_012"> among such groups</span></div>
<div style="position:absolute;left:62.08px;top:398.48px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Constraints specify some restrictions on valid data; these</span></div>
<div style="position:absolute;left:84.58px;top:424.64px" class="cls_012"><span class="cls_012">constraints must be enforced at all times</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Data Models (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Data Model Operations:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  These operations are used for specifying database</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_017"><span class="cls_017">retrievals</span><span class="cls_016"> and </span><span class="cls_017">updates</span><span class="cls_016"> by referring to the</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">constructs of the data model.</span></div>
<div style="position:absolute;left:62.08px;top:268.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Operations on the data model may include </span><span class="cls_018">basic</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_018"><span class="cls_018">model operations </span><span class="cls_016">(e.g. generic insert, delete,</span></div>
<div style="position:absolute;left:84.58px;top:330.64px" class="cls_016"><span class="cls_016">update) and</span><span class="cls_018"> user-defined operations </span><span class="cls_016">(e.g.</span></div>
<div style="position:absolute;left:84.58px;top:362.56px" class="cls_016"><span class="cls_016">compute_student_gpa, update_inventory)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Categories of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:101.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Conceptual (high-level, semantic) data models:</span></div>
<div style="position:absolute;left:62.08px;top:133.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Provide concepts that are close to the way many users</span></div>
<div style="position:absolute;left:84.58px;top:157.52px" class="cls_012"><span class="cls_012">perceive data.</span></div>
<div style="position:absolute;left:98.08px;top:196.64px" class="cls_019"><span class="cls_019">n</span></div>
<div style="position:absolute;left:116.07px;top:186.64px" class="cls_020"><span class="cls_020">(Also called </span><span class="cls_021">entity-based</span><span class="cls_020"> or</span><span class="cls_021"> object-based</span><span class="cls_020"> data models.)</span></div>
<div style="position:absolute;left:26.08px;top:212.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Physical (low-level, internal) data models:</span></div>
<div style="position:absolute;left:62.08px;top:244.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Provide concepts that describe details of how data is stored</span></div>
<div style="position:absolute;left:84.58px;top:268.64px" class="cls_012"><span class="cls_012">in the computer. These are usually specified in an ad-hoc</span></div>
<div style="position:absolute;left:84.58px;top:291.68px" class="cls_012"><span class="cls_012">manner through DBMS design and administration manuals</span></div>
<div style="position:absolute;left:26.08px;top:321.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Implementation (representational) data models:</span></div>
<div style="position:absolute;left:62.08px;top:352.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Provide concepts that fall between the above two, used by</span></div>
<div style="position:absolute;left:84.58px;top:376.64px" class="cls_012"><span class="cls_012">many commercial DBMS implementations (e.g. relational</span></div>
<div style="position:absolute;left:84.58px;top:400.64px" class="cls_012"><span class="cls_012">data models used in many commercial systems).</span></div>
<div style="position:absolute;left:26.08px;top:429.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Self-Describing Data Models:</span></div>
<div style="position:absolute;left:62.08px;top:460.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Combine the description of data with the data values.</span></div>
<div style="position:absolute;left:84.58px;top:484.64px" class="cls_012"><span class="cls_012">Examples include XML, key-value stores and some NOSQL</span></div>
<div style="position:absolute;left:84.58px;top:508.64px" class="cls_012"><span class="cls_012">systems.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Schemas versus Instances</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database Schema:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The </span><span class="cls_018">description</span><span class="cls_016"> of a database.</span></div>
<div style="position:absolute;left:62.08px;top:196.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Includes descriptions of the database structure,</span></div>
<div style="position:absolute;left:84.58px;top:224.56px" class="cls_016"><span class="cls_016">data types, and the constraints on the database.</span></div>
<div style="position:absolute;left:26.08px;top:259.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Schema Diagram:</span></div>
<div style="position:absolute;left:62.08px;top:295.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  An </span><span class="cls_018">illustrative</span><span class="cls_016"> display of (most aspects of) a</span></div>
<div style="position:absolute;left:84.58px;top:323.68px" class="cls_016"><span class="cls_016">database schema.</span></div>
<div style="position:absolute;left:26.08px;top:358.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Schema Construct:</span></div>
<div style="position:absolute;left:62.08px;top:395.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  A </span><span class="cls_018">component</span><span class="cls_016"> of the schema or an object within</span></div>
<div style="position:absolute;left:84.58px;top:423.52px" class="cls_016"><span class="cls_016">the schema, e.g., STUDENT, COURSE.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Schemas versus Instances</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database State:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The actual data stored in a database at a</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_018"><span class="cls_018">particular moment in time</span><span class="cls_016">. This includes the</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">collection of all the data in the database.</span></div>
<div style="position:absolute;left:62.08px;top:268.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Also called database instance (or occurrence or</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_016"><span class="cls_016">snapshot).</span></div>
<div style="position:absolute;left:98.08px;top:336.48px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> The term </span><span class="cls_024">instance </span><span class="cls_023">is also applied to individual</span></div>
<div style="position:absolute;left:116.07px;top:365.52px" class="cls_023"><span class="cls_023">database components, e.g. </span><span class="cls_024">record instance, table</span></div>
<div style="position:absolute;left:116.07px;top:394.56px" class="cls_024"><span class="cls_024">instance, entity instance</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Database Schema</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">vs. Database State</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database State:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Refers to the </span><span class="cls_018">content</span><span class="cls_016"> of a database at a moment</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">in time.</span></div>
<div style="position:absolute;left:26.08px;top:237.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Initial Database State:</span></div>
<div style="position:absolute;left:62.08px;top:277.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Refers to the database state when it is initially</span></div>
<div style="position:absolute;left:84.58px;top:308.56px" class="cls_016"><span class="cls_016">loaded into the system.</span></div>
<div style="position:absolute;left:26.08px;top:346.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Valid State:</span></div>
<div style="position:absolute;left:62.08px;top:386.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  A state that satisfies the structure and constraints</span></div>
<div style="position:absolute;left:84.58px;top:417.52px" class="cls_016"><span class="cls_016">of the database.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Database Schema</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">vs. Database State (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Distinction</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The </span><span class="cls_018">database schema</span><span class="cls_016"> changes very infrequently.</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The </span><span class="cls_018">database state</span><span class="cls_016"> changes every time the</span></div>
<div style="position:absolute;left:84.58px;top:237.52px" class="cls_016"><span class="cls_016">database is updated.</span></div>
<div style="position:absolute;left:26.08px;top:312.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Schema</span><span class="cls_008"> is also called </span><span class="cls_014">intension</span><span class="cls_008">.</span></div>
<div style="position:absolute;left:26.08px;top:352.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  State</span><span class="cls_008"> is also called </span><span class="cls_014">extension</span><span class="cls_008">.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example of a Database Schema</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:620.47px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example of a database state</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Three-Schema Architecture</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Proposed to support DBMS characteristics of:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_025">  Program-data independence.</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Support of </span><span class="cls_025">multiple views</span><span class="cls_016"> of the data.</span></div>
<div style="position:absolute;left:26.08px;top:243.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Not explicitly used in commercial DBMS products,</span></div>
<div style="position:absolute;left:53.08px;top:277.52px" class="cls_008"><span class="cls_008">but has been useful in explaining database</span></div>
<div style="position:absolute;left:53.08px;top:311.60px" class="cls_008"><span class="cls_008">system organization</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Three-Schema Architecture</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Defines DBMS schemas at </span><span class="cls_026">three</span><span class="cls_023"> levels:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_027">  Internal schema</span><span class="cls_012"> at the internal level to describe physical</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">storage structures and access paths (e.g indexes).</span></div>
<div style="position:absolute;left:98.08px;top:220.48px" class="cls_019"><span class="cls_019">n</span><span class="cls_020">  Typically uses a </span><span class="cls_028">physical</span><span class="cls_020"> data model.</span></div>
<div style="position:absolute;left:62.08px;top:249.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_027">  Conceptual schema</span><span class="cls_012"> at the conceptual level to describe the</span></div>
<div style="position:absolute;left:84.58px;top:276.56px" class="cls_012"><span class="cls_012">structure and constraints for the whole database for a</span></div>
<div style="position:absolute;left:84.58px;top:302.48px" class="cls_012"><span class="cls_012">community of users.</span></div>
<div style="position:absolute;left:98.08px;top:333.52px" class="cls_019"><span class="cls_019">n</span><span class="cls_020">  Uses a </span><span class="cls_028">conceptual</span><span class="cls_020"> or an </span><span class="cls_028">implementation</span><span class="cls_020"> data model.</span></div>
<div style="position:absolute;left:62.08px;top:363.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_027">  External schemas</span><span class="cls_012"> at the external level to describe the</span></div>
<div style="position:absolute;left:84.58px;top:389.60px" class="cls_012"><span class="cls_012">various user views.</span></div>
<div style="position:absolute;left:98.08px;top:420.64px" class="cls_019"><span class="cls_019">n</span><span class="cls_020">  Usually uses the same data model as the </span><span class="cls_028">conceptual</span></div>
<div style="position:absolute;left:116.07px;top:444.64px" class="cls_020"><span class="cls_020">schema.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">The three-schema architecture</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Three-Schema Architecture</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Mappings among schema levels are needed to</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">transform requests and data.</span></div>
<div style="position:absolute;left:62.08px;top:202.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Programs refer to an external schema, and are</span></div>
<div style="position:absolute;left:84.58px;top:233.68px" class="cls_016"><span class="cls_016">mapped by the DBMS to the internal schema for</span></div>
<div style="position:absolute;left:84.58px;top:264.64px" class="cls_016"><span class="cls_016">execution.</span></div>
<div style="position:absolute;left:62.08px;top:302.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Data extracted from the internal DBMS level is</span></div>
<div style="position:absolute;left:84.58px;top:333.52px" class="cls_016"><span class="cls_016">reformatted to match the user’s external view (e.g.</span></div>
<div style="position:absolute;left:84.58px;top:364.48px" class="cls_016"><span class="cls_016">formatting the results of an SQL query for display</span></div>
<div style="position:absolute;left:84.58px;top:395.68px" class="cls_016"><span class="cls_016">in a Web page)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Data Independence</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Logical Data Independence:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The capacity to change the </span><span class="cls_029">conceptual</span><span class="cls_016"> schema</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_016"><span class="cls_016">without having to change the </span><span class="cls_030">external</span><span class="cls_016"> schemas</span></div>
<div style="position:absolute;left:84.58px;top:218.56px" class="cls_016"><span class="cls_016">and their associated application programs.</span></div>
<div style="position:absolute;left:26.08px;top:252.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Physical Data Independence:</span></div>
<div style="position:absolute;left:62.08px;top:289.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The capacity to change the </span><span class="cls_029">internal</span><span class="cls_016"> schema</span></div>
<div style="position:absolute;left:84.58px;top:317.68px" class="cls_016"><span class="cls_016">without having to change the </span><span class="cls_030">conceptual</span><span class="cls_016"> schema.</span></div>
<div style="position:absolute;left:62.08px;top:351.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  For example, the internal schema may be changed</span></div>
<div style="position:absolute;left:84.58px;top:380.56px" class="cls_016"><span class="cls_016">when certain file structures are reorganized or new</span></div>
<div style="position:absolute;left:84.58px;top:408.64px" class="cls_016"><span class="cls_016">indexes are created to improve database</span></div>
<div style="position:absolute;left:84.58px;top:436.48px" class="cls_016"><span class="cls_016">performance</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Data Independence (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  When a schema at a lower level is changed, only</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">the </span><span class="cls_014">mappings</span><span class="cls_008"> between this schema and higher-</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_008"><span class="cls_008">level schemas need to be changed in a DBMS</span></div>
<div style="position:absolute;left:53.08px;top:229.52px" class="cls_008"><span class="cls_008">that fully supports data independence.</span></div>
<div style="position:absolute;left:26.08px;top:269.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  The higher-level schemas themselves are</span></div>
<div style="position:absolute;left:53.08px;top:303.68px" class="cls_014"><span class="cls_014">unchanged</span><span class="cls_008">.</span></div>
<div style="position:absolute;left:62.08px;top:343.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Hence, the application programs need not be</span></div>
<div style="position:absolute;left:84.58px;top:374.56px" class="cls_016"><span class="cls_016">changed since they refer to the external schemas.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">DBMS Languages</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Definition Language (DDL)</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Manipulation Language (DML)</span></div>
<div style="position:absolute;left:62.08px;top:208.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  High-Level or Non-procedural Languages: These</span></div>
<div style="position:absolute;left:84.58px;top:240.64px" class="cls_016"><span class="cls_016">include the relational language SQL</span></div>
<div style="position:absolute;left:98.08px;top:277.68px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> May be used in a standalone way or may be</span></div>
<div style="position:absolute;left:116.07px;top:305.52px" class="cls_023"><span class="cls_023">embedded in a programming language</span></div>
<div style="position:absolute;left:62.08px;top:340.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Low Level or Procedural Languages:</span></div>
<div style="position:absolute;left:98.08px;top:377.52px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> These must be embedded in a programming</span></div>
<div style="position:absolute;left:116.07px;top:406.56px" class="cls_023"><span class="cls_023">language</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">DBMS Languages</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Data Definition Language (DDL):</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Used by the DBA and database designers to</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_016"><span class="cls_016">specify the </span><span class="cls_029">conceptual schema </span><span class="cls_016">of a database.</span></div>
<div style="position:absolute;left:62.08px;top:224.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  In many DBMSs, the DDL is also used to define</span></div>
<div style="position:absolute;left:84.58px;top:252.64px" class="cls_016"><span class="cls_016">internal and external schemas (views).</span></div>
<div style="position:absolute;left:62.08px;top:286.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  In some DBMSs, separate </span><span class="cls_025">storage definition</span></div>
<div style="position:absolute;left:84.58px;top:315.52px" class="cls_025"><span class="cls_025">language (SDL) </span><span class="cls_016">and</span><span class="cls_025"> view definition language</span></div>
<div style="position:absolute;left:84.58px;top:343.60px" class="cls_025"><span class="cls_025">(VDL)</span><span class="cls_016"> are used to define internal and external</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_016"><span class="cls_016">schemas.</span></div>
<div style="position:absolute;left:98.08px;top:405.60px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> SDL is typically realized via DBMS commands</span></div>
<div style="position:absolute;left:116.07px;top:431.52px" class="cls_023"><span class="cls_023">provided to the DBA and database designers</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">DBMS Languages</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Data Manipulation Language (DML):</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Used to specify database </span><span class="cls_029">retrievals</span><span class="cls_016"> and </span><span class="cls_029">updates</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  DML commands (data sublanguage) can be</span></div>
<div style="position:absolute;left:84.58px;top:237.52px" class="cls_017"><span class="cls_017">embedded</span><span class="cls_016"> in a general-purpose programming</span></div>
<div style="position:absolute;left:84.58px;top:268.48px" class="cls_016"><span class="cls_016">language (host language), such as COBOL, C,</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_016"><span class="cls_016">C++, or Java.</span></div>
<div style="position:absolute;left:98.08px;top:336.48px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> A library of functions can also be provided to access</span></div>
<div style="position:absolute;left:116.07px;top:365.52px" class="cls_023"><span class="cls_023">the DBMS from a programming language</span></div>
<div style="position:absolute;left:62.08px;top:400.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Alternatively, stand-alone DML commands can be</span></div>
<div style="position:absolute;left:84.58px;top:431.68px" class="cls_016"><span class="cls_016">applied directly (called a </span><span class="cls_017">query language</span><span class="cls_016">).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Types of DML</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  High Level or Non-procedural Language:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  For example, the SQL relational language</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Are “set”-oriented and specify what data to retrieve</span></div>
<div style="position:absolute;left:84.58px;top:237.52px" class="cls_016"><span class="cls_016">rather than how to retrieve it.</span></div>
<div style="position:absolute;left:62.08px;top:274.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Also called </span><span class="cls_025">declarative</span><span class="cls_016"> languages.</span></div>
<div style="position:absolute;left:26.08px;top:312.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Low Level or Procedural Language:</span></div>
<div style="position:absolute;left:62.08px;top:352.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Retrieve data one record-at-a-time;</span></div>
<div style="position:absolute;left:62.08px;top:389.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Constructs such as looping are needed to retrieve</span></div>
<div style="position:absolute;left:84.58px;top:421.60px" class="cls_016"><span class="cls_016">multiple records, along with positioning pointers.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">DBMS Interfaces</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Stand-alone query language interfaces</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Example: Entering SQL queries at the DBMS</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">interactive SQL interface (e.g. SQL*Plus in</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">ORACLE)</span></div>
<div style="position:absolute;left:26.08px;top:268.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Programmer interfaces for embedding DML in</span></div>
<div style="position:absolute;left:53.08px;top:302.48px" class="cls_008"><span class="cls_008">programming languages</span></div>
<div style="position:absolute;left:26.08px;top:342.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  User-friendly interfaces</span></div>
<div style="position:absolute;left:62.08px;top:382.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Menu-based, forms-based, graphics-based, etc.</span></div>
<div style="position:absolute;left:26.08px;top:420.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Mobile Interfaces:interfaces allowing users to</span></div>
<div style="position:absolute;left:53.08px;top:454.64px" class="cls_008"><span class="cls_008">perform transactions using mobile apps</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_031"><span class="cls_031">DBMS Programming Language Interfaces</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Programmer interfaces for embedding DML in a</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">programming languages:</span></div>
<div style="position:absolute;left:62.08px;top:200.56px" class="cls_032"><span class="cls_032">n</span><span class="cls_033">   Embedded Approach</span><span class="cls_034">: e.g embedded SQL (for C, C++, etc.),</span></div>
<div style="position:absolute;left:84.58px;top:224.56px" class="cls_034"><span class="cls_034">SQLJ (for Java)</span></div>
<div style="position:absolute;left:62.08px;top:253.60px" class="cls_032"><span class="cls_032">n</span><span class="cls_033">   Procedure Call Approach</span><span class="cls_034">: e.g. JDBC for Java, ODBC (Open</span></div>
<div style="position:absolute;left:84.58px;top:277.60px" class="cls_034"><span class="cls_034">Databse Connectivity) for other programming languages as API’s</span></div>
<div style="position:absolute;left:84.58px;top:301.60px" class="cls_034"><span class="cls_034">(application programming interfaces)</span></div>
<div style="position:absolute;left:62.08px;top:330.64px" class="cls_032"><span class="cls_032">n</span><span class="cls_033">   Database Programming Language Approach</span><span class="cls_034">: e.g. ORACLE</span></div>
<div style="position:absolute;left:84.58px;top:354.64px" class="cls_034"><span class="cls_034">has PL/SQL, a programming language based on SQL; language</span></div>
<div style="position:absolute;left:84.58px;top:378.64px" class="cls_034"><span class="cls_034">incorporates SQL and its data types as integral components</span></div>
<div style="position:absolute;left:62.08px;top:407.68px" class="cls_032"><span class="cls_032">n</span><span class="cls_033">   Scripting Languages: </span><span class="cls_034">PHP (client-side scripting) and Python</span></div>
<div style="position:absolute;left:84.58px;top:431.68px" class="cls_034"><span class="cls_034">(server-side scripting) are used to write database programs.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">User-Friendly DBMS Interfaces</span></div>
<div style="position:absolute;left:62.08px;top:144.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Menu-based (Web-based), popular for browsing</span></div>
<div style="position:absolute;left:84.58px;top:175.60px" class="cls_016"><span class="cls_016">on the web</span></div>
<div style="position:absolute;left:62.08px;top:213.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Forms-based, designed for naïve users used to</span></div>
<div style="position:absolute;left:84.58px;top:244.48px" class="cls_016"><span class="cls_016">filling in entries on a form</span></div>
<div style="position:absolute;left:62.08px;top:281.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Graphics-based</span></div>
<div style="position:absolute;left:98.08px;top:318.48px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Point and Click, Drag and Drop, etc.</span></div>
<div style="position:absolute;left:98.08px;top:353.52px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Specifying a query on a schema diagram</span></div>
<div style="position:absolute;left:62.08px;top:388.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Natural language: requests in written English</span></div>
<div style="position:absolute;left:62.08px;top:425.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Combinations of the above:</span></div>
<div style="position:absolute;left:98.08px;top:462.48px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> For example, both menus and forms used</span></div>
<div style="position:absolute;left:116.07px;top:491.52px" class="cls_023"><span class="cls_023">extensively in Web database interfaces</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Other DBMS Interfaces</span></div>
<div style="position:absolute;left:62.08px;top:128.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Natural language: free text as a query</span></div>
<div style="position:absolute;left:62.08px;top:166.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Speech : Input query and Output response</span></div>
<div style="position:absolute;left:62.08px;top:203.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Web Browser with keyword search</span></div>
<div style="position:absolute;left:62.08px;top:241.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Parametric interfaces, e.g., bank tellers using</span></div>
<div style="position:absolute;left:84.58px;top:272.56px" class="cls_016"><span class="cls_016">function keys.</span></div>
<div style="position:absolute;left:62.08px;top:309.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Interfaces for the DBA:</span></div>
<div style="position:absolute;left:98.08px;top:346.56px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Creating user accounts, granting authorizations</span></div>
<div style="position:absolute;left:98.08px;top:381.60px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Setting system parameters</span></div>
<div style="position:absolute;left:98.08px;top:415.68px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Changing schemas or access paths</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Database System Utilities</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  To perform certain functions such as:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Loading data stored in files into a database.</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">Includes data conversion tools.</span></div>
<div style="position:absolute;left:62.08px;top:237.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Backing up the database periodically on tape.</span></div>
<div style="position:absolute;left:62.08px;top:274.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Reorganizing database file structures.</span></div>
<div style="position:absolute;left:62.08px;top:312.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Performance monitoring utilities.</span></div>
<div style="position:absolute;left:62.08px;top:349.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Report generation utilities.</span></div>
<div style="position:absolute;left:62.08px;top:387.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Other functions, such as sorting, user monitoring,</span></div>
<div style="position:absolute;left:84.58px;top:418.48px" class="cls_016"><span class="cls_016">data compression, etc.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Other Tools</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data dictionary / repository:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Used to store schema descriptions and other</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">information such as design decisions, application</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">program descriptions, user information, usage</span></div>
<div style="position:absolute;left:84.58px;top:262.48px" class="cls_016"><span class="cls_016">standards, etc.</span></div>
<div style="position:absolute;left:62.08px;top:299.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_025">  Active data dictionary</span><span class="cls_016"> is accessed by DBMS</span></div>
<div style="position:absolute;left:84.58px;top:330.64px" class="cls_016"><span class="cls_016">software and users/DBA.</span></div>
<div style="position:absolute;left:62.08px;top:368.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_025">  Passive data dictionary</span><span class="cls_016"> is accessed by</span></div>
<div style="position:absolute;left:84.58px;top:399.52px" class="cls_016"><span class="cls_016">users/DBA only.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Other Tools</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Application Development Environments and</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">CASE (computer-aided software engineering)</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_008"><span class="cls_008">tools:</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Examples:</span></div>
<div style="position:absolute;left:62.08px;top:276.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  PowerBuilder (Sybase)</span></div>
<div style="position:absolute;left:62.08px;top:313.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  JBuilder (Borland)</span></div>
<div style="position:absolute;left:62.08px;top:351.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  JDeveloper 10G (Oracle)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Typical DBMS Component Modules</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Centralized and</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Client-Server DBMS Architectures</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Centralized DBMS:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Combines everything into single system including-</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">DBMS software, hardware, application programs,</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">and user interface processing software.</span></div>
<div style="position:absolute;left:62.08px;top:268.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  User can still connect through a remote terminal -</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_016"><span class="cls_016">however, all processing is done at centralized site.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">A Physical Centralized Architecture</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_031"><span class="cls_031">Basic 2-tier Client-Server Architectures</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Specialized Servers with Specialized functions</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Print server</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  File server</span></div>
<div style="position:absolute;left:62.08px;top:243.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  DBMS server</span></div>
<div style="position:absolute;left:62.08px;top:280.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Web server</span></div>
<div style="position:absolute;left:62.08px;top:318.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Email server</span></div>
<div style="position:absolute;left:26.08px;top:356.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Clients can access the specialized servers as</span></div>
<div style="position:absolute;left:53.08px;top:389.60px" class="cls_008"><span class="cls_008">needed</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_031"><span class="cls_031">Logical two-tier client server architecture</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Clients</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Provide appropriate interfaces through a client</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">software module to access and utilize the various</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_008"><span class="cls_008">server resources.</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Clients may be diskless machines or PCs or</span></div>
<div style="position:absolute;left:53.08px;top:269.60px" class="cls_008"><span class="cls_008">Workstations with disks with only the client</span></div>
<div style="position:absolute;left:53.08px;top:303.68px" class="cls_008"><span class="cls_008">software installed.</span></div>
<div style="position:absolute;left:26.08px;top:343.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Connected to the servers via some form of a</span></div>
<div style="position:absolute;left:53.08px;top:377.60px" class="cls_008"><span class="cls_008">network.</span></div>
<div style="position:absolute;left:62.08px;top:417.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  (LAN: local area network, wireless network, etc.)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">DBMS Server</span></div>
<div style="position:absolute;left:26.08px;top:125.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Provides database query and transaction services to the</span></div>
<div style="position:absolute;left:53.08px;top:151.68px" class="cls_023"><span class="cls_023">clients</span></div>
<div style="position:absolute;left:26.08px;top:183.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Relational DBMS servers are often called SQL servers,</span></div>
<div style="position:absolute;left:53.08px;top:209.52px" class="cls_023"><span class="cls_023">query servers, or transaction servers</span></div>
<div style="position:absolute;left:26.08px;top:241.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Applications running on clients utilize an Application</span></div>
<div style="position:absolute;left:53.08px;top:267.60px" class="cls_023"><span class="cls_023">Program Interface (</span><span class="cls_010">API</span><span class="cls_023">) to access server databases via</span></div>
<div style="position:absolute;left:53.08px;top:293.52px" class="cls_023"><span class="cls_023">standard interface such as:</span></div>
<div style="position:absolute;left:62.08px;top:324.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  ODBC: Open Database Connectivity standard</span></div>
<div style="position:absolute;left:62.08px;top:353.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  JDBC: for Java programming access</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Two Tier Client-Server Architecture</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Client and server must install appropriate client</span></div>
<div style="position:absolute;left:53.08px;top:155.60px" class="cls_008"><span class="cls_008">module and server module software for ODBC or</span></div>
<div style="position:absolute;left:53.08px;top:185.60px" class="cls_008"><span class="cls_008">JDBC</span></div>
<div style="position:absolute;left:26.08px;top:226.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  A client program may connect to several DBMSs,</span></div>
<div style="position:absolute;left:53.08px;top:259.52px" class="cls_008"><span class="cls_008">sometimes called the data sources.</span></div>
<div style="position:absolute;left:26.08px;top:300.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  In general, data sources can be files or other</span></div>
<div style="position:absolute;left:53.08px;top:333.68px" class="cls_008"><span class="cls_008">non-DBMS software that manages data.</span></div>
<div style="position:absolute;left:26.08px;top:374.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  See Chapter 10 for details on Database</span></div>
<div style="position:absolute;left:53.08px;top:407.60px" class="cls_008"><span class="cls_008">Programming</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Three Tier Client-Server Architecture</span></div>
<div style="position:absolute;left:26.08px;top:104.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Common for Web applications</span></div>
<div style="position:absolute;left:26.08px;top:139.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Intermediate Layer called Application Server or Web</span></div>
<div style="position:absolute;left:53.08px;top:168.48px" class="cls_023"><span class="cls_023">Server:</span></div>
<div style="position:absolute;left:62.08px;top:202.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Stores the web connectivity software and the business logic</span></div>
<div style="position:absolute;left:84.58px;top:228.56px" class="cls_012"><span class="cls_012">part of the application used to access the corresponding</span></div>
<div style="position:absolute;left:84.58px;top:255.68px" class="cls_012"><span class="cls_012">data from the database server</span></div>
<div style="position:absolute;left:62.08px;top:286.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Acts like a conduit for sending partially processed data</span></div>
<div style="position:absolute;left:84.58px;top:313.52px" class="cls_012"><span class="cls_012">between the database server and the client.</span></div>
<div style="position:absolute;left:26.08px;top:345.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Three-tier Architecture Can Enhance Security:</span></div>
<div style="position:absolute;left:62.08px;top:379.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Database server only accessible via middle tier</span></div>
<div style="position:absolute;left:62.08px;top:411.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Clients cannot directly access database server</span></div>
<div style="position:absolute;left:62.08px;top:442.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Clients contain user interfaces and Web browsers</span></div>
<div style="position:absolute;left:62.08px;top:474.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  The client is typically a PC or a mobile device connected to</span></div>
<div style="position:absolute;left:84.58px;top:500.48px" class="cls_012"><span class="cls_012">the Web</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Three-tier client-server architecture</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Classification of DBMSs</span></div>
<div style="position:absolute;left:7.20px;top:101.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Based on the data model used</span></div>
<div style="position:absolute;left:43.20px;top:138.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Legacy: Network, Hierarchical.</span></div>
<div style="position:absolute;left:43.20px;top:172.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Currently Used: Relational, Object-oriented, Object-</span></div>
<div style="position:absolute;left:65.70px;top:200.56px" class="cls_016"><span class="cls_016">relational</span></div>
<div style="position:absolute;left:43.20px;top:234.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Recent Technologies: Key-value storage systems,</span></div>
<div style="position:absolute;left:65.70px;top:262.48px" class="cls_016"><span class="cls_016">NOSQL systems: document based, column-based,</span></div>
<div style="position:absolute;left:65.70px;top:291.52px" class="cls_016"><span class="cls_016">graph-based and key-value based. Native XML</span></div>
<div style="position:absolute;left:65.70px;top:319.60px" class="cls_016"><span class="cls_016">DBMSs.</span></div>
<div style="position:absolute;left:7.20px;top:353.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Other classifications</span></div>
<div style="position:absolute;left:43.20px;top:390.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Single-user (typically used with personal computers)</span></div>
<div style="position:absolute;left:65.70px;top:418.48px" class="cls_016"><span class="cls_016">vs. multi-user (most DBMSs).</span></div>
<div style="position:absolute;left:43.20px;top:452.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Centralized (uses a single computer with one</span></div>
<div style="position:absolute;left:65.70px;top:480.64px" class="cls_016"><span class="cls_016">database) vs. distributed (multiple computers, multiple</span></div>
<div style="position:absolute;left:65.70px;top:508.48px" class="cls_016"><span class="cls_016">DBs)</span><span class="cls_002">2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Variations of Distributed DBMSs</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">(DDBMSs)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Homogeneous DDBMS</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Heterogeneous DDBMS</span></div>
<div style="position:absolute;left:26.08px;top:209.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Federated or Multidatabase Systems</span></div>
<div style="position:absolute;left:62.08px;top:249.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Participating Databases are loosely coupled with</span></div>
<div style="position:absolute;left:84.58px;top:280.48px" class="cls_016"><span class="cls_016">high degree of autonomy.</span></div>
<div style="position:absolute;left:26.08px;top:318.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Distributed Database Systems have now come to</span></div>
<div style="position:absolute;left:53.08px;top:351.68px" class="cls_008"><span class="cls_008">be known as client-server based database</span></div>
<div style="position:absolute;left:53.08px;top:385.52px" class="cls_008"><span class="cls_008">systems because:</span></div>
<div style="position:absolute;left:62.08px;top:425.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  They do not support a totally distributed</span></div>
<div style="position:absolute;left:84.58px;top:456.64px" class="cls_016"><span class="cls_016">environment, but rather a set of database servers</span></div>
<div style="position:absolute;left:84.58px;top:487.60px" class="cls_016"><span class="cls_016">supporting a set of clients.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Cost considerations for DBMSs</span></div>
<div style="position:absolute;left:26.08px;top:125.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Cost Range: from free open-source systems to</span></div>
<div style="position:absolute;left:53.08px;top:151.68px" class="cls_023"><span class="cls_023">configurations costing millions of dollars</span></div>
<div style="position:absolute;left:26.08px;top:183.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Examples of free relational DBMSs: MySQL, PostgreSQL,</span></div>
<div style="position:absolute;left:53.08px;top:209.52px" class="cls_023"><span class="cls_023">others</span></div>
<div style="position:absolute;left:26.08px;top:241.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Commercial DBMS offer additional specialized modules,</span></div>
<div style="position:absolute;left:53.08px;top:267.60px" class="cls_023"><span class="cls_023">e.g. time-series module, spatial data module, document</span></div>
<div style="position:absolute;left:53.08px;top:293.52px" class="cls_023"><span class="cls_023">module, XML module</span></div>
<div style="position:absolute;left:62.08px;top:324.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  These offer additional specialized functionality when</span></div>
<div style="position:absolute;left:84.58px;top:348.56px" class="cls_012"><span class="cls_012">purchased separately</span></div>
<div style="position:absolute;left:62.08px;top:377.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Sometimes called cartridges (e.g., in Oracle) or blades</span></div>
<div style="position:absolute;left:26.08px;top:406.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Different licensing options: site license, maximum number</span></div>
<div style="position:absolute;left:53.08px;top:432.48px" class="cls_023"><span class="cls_023">of concurrent users (seat license), single user, etc.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Other Considerations</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Type of access paths within database system</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  E.g.- inverted indexing based (ADABAS is one</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">such system).Fully indexed databases provide</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">access by any keyword (used in search engines)</span></div>
<div style="position:absolute;left:26.08px;top:268.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  General Purpose vs. Special Purpose</span></div>
<div style="position:absolute;left:62.08px;top:308.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  E.g.- Airline Reservation systems or many others-</span></div>
<div style="position:absolute;left:84.58px;top:339.52px" class="cls_016"><span class="cls_016">reservation systems for hotel/car etc.  are special</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_016"><span class="cls_016">purpose OLTP (Online Transaction Processing</span></div>
<div style="position:absolute;left:84.58px;top:402.64px" class="cls_016"><span class="cls_016">Systems)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:8.16px" class="cls_006"><span class="cls_006">History of Data Models (Additional</span></div>
<div style="position:absolute;left:25.20px;top:51.36px" class="cls_006"><span class="cls_006">Material)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Network Model</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Hierarchical Model</span></div>
<div style="position:absolute;left:26.08px;top:209.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Relational Model</span></div>
<div style="position:absolute;left:26.08px;top:249.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Object-oriented Data Models</span></div>
<div style="position:absolute;left:26.08px;top:290.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Object-Relational Models</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Network Model:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The first network DBMS was implemented by</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_016"><span class="cls_016">Honeywell in 1964-65 (IDS System).</span></div>
<div style="position:absolute;left:62.08px;top:224.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Adopted heavily due to the support by CODASYL</span></div>
<div style="position:absolute;left:84.58px;top:252.64px" class="cls_016"><span class="cls_016">(Conference on Data Systems Languages)</span></div>
<div style="position:absolute;left:84.58px;top:280.48px" class="cls_016"><span class="cls_016">(CODASYL - DBTG report of 1971).</span></div>
<div style="position:absolute;left:62.08px;top:315.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Later implemented in a large variety of systems -</span></div>
<div style="position:absolute;left:84.58px;top:343.60px" class="cls_016"><span class="cls_016">IDMS (Cullinet - now Computer Associates), DMS</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_016"><span class="cls_016">1100 (Unisys), IMAGE (H.P. (Hewlett-Packard)),</span></div>
<div style="position:absolute;left:84.58px;top:399.52px" class="cls_016"><span class="cls_016">VAX -DBMS (Digital Equipment Corp., next</span></div>
<div style="position:absolute;left:84.58px;top:427.60px" class="cls_016"><span class="cls_016">COMPAQ, now H.P.).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 45</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Network Model</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Network Model</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Represents data as record types</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Represents a limited type of 1:N relationship</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 46</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background47.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Network Model</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Advantages:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Network Model is able to model complex</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_016"><span class="cls_016">relationships and represents semantics of</span></div>
<div style="position:absolute;left:84.58px;top:218.56px" class="cls_016"><span class="cls_016">add/delete on the relationships.</span></div>
<div style="position:absolute;left:62.08px;top:252.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Can handle most situations for modeling using</span></div>
<div style="position:absolute;left:84.58px;top:280.48px" class="cls_016"><span class="cls_016">record types and relationship types.</span></div>
<div style="position:absolute;left:62.08px;top:315.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Language is navigational; uses constructs like</span></div>
<div style="position:absolute;left:84.58px;top:343.60px" class="cls_016"><span class="cls_016">FIND, FIND member, FIND owner, FIND NEXT</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_016"><span class="cls_016">within set, GET, etc.</span></div>
<div style="position:absolute;left:98.08px;top:405.60px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Programmers can do optimal navigation through the</span></div>
<div style="position:absolute;left:116.07px;top:431.52px" class="cls_023"><span class="cls_023">database.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 47</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background48.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Network Model</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Disadvantages:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Navigational and procedural nature of processing</span></div>
<div style="position:absolute;left:62.08px;top:206.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Database contains a complex array of pointers</span></div>
<div style="position:absolute;left:84.58px;top:237.52px" class="cls_016"><span class="cls_016">that thread through a set of records.</span></div>
<div style="position:absolute;left:98.08px;top:274.56px" class="cls_022"><span class="cls_022">n</span><span class="cls_023"> Little scope for automated “query optimization”</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 48</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background49.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Hierarchical Data Model:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Initially implemented in a joint effort by IBM and</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">North American Rockwell around 1965. Resulted</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_016"><span class="cls_016">in the IMS family of systems.</span></div>
<div style="position:absolute;left:62.08px;top:268.48px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  IBM’s IMS product had (and still has) a very large</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_016"><span class="cls_016">customer base worldwide</span></div>
<div style="position:absolute;left:62.08px;top:337.60px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Hierarchical model was formalized based on the</span></div>
<div style="position:absolute;left:84.58px;top:368.56px" class="cls_016"><span class="cls_016">IMS system</span></div>
<div style="position:absolute;left:62.08px;top:405.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Other systems based on this model: System 2k</span></div>
<div style="position:absolute;left:84.58px;top:436.48px" class="cls_016"><span class="cls_016">(SAS inc.)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 49</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background50.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Hierarchical Model</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Advantages:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Simple to construct and operate</span></div>
<div style="position:absolute;left:62.08px;top:194.48px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Corresponds to a number of natural hierarchically organized</span></div>
<div style="position:absolute;left:84.58px;top:221.60px" class="cls_012"><span class="cls_012">domains, e.g., organization (“org”) chart</span></div>
<div style="position:absolute;left:62.08px;top:252.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Language is simple:</span></div>
<div style="position:absolute;left:98.08px;top:283.60px" class="cls_019"><span class="cls_019">n</span><span class="cls_020">  Uses constructs like GET, GET UNIQUE, GET NEXT, GET</span></div>
<div style="position:absolute;left:116.07px;top:307.60px" class="cls_020"><span class="cls_020">NEXT WITHIN PARENT, etc.</span></div>
<div style="position:absolute;left:26.08px;top:337.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_023">  Disadvantages:</span></div>
<div style="position:absolute;left:62.08px;top:371.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Navigational and procedural nature of processing</span></div>
<div style="position:absolute;left:62.08px;top:403.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Database is visualized as a linear arrangement of records</span></div>
<div style="position:absolute;left:62.08px;top:435.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Little scope for "query optimization"</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 50</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:27500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background51.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Relational Model:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Proposed in 1970 by E.F. Codd (IBM), first commercial</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">system in 1981-82.</span></div>
<div style="position:absolute;left:62.08px;top:221.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Now in several commercial products (e.g. DB2, ORACLE,</span></div>
<div style="position:absolute;left:84.58px;top:247.52px" class="cls_012"><span class="cls_012">MS SQL Server, SYBASE, INFORMIX).</span></div>
<div style="position:absolute;left:62.08px;top:279.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Several free open source implementations, e.g. MySQL,</span></div>
<div style="position:absolute;left:84.58px;top:305.60px" class="cls_012"><span class="cls_012">PostgreSQL</span></div>
<div style="position:absolute;left:62.08px;top:337.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Currently most dominant for developing database</span></div>
<div style="position:absolute;left:84.58px;top:363.68px" class="cls_012"><span class="cls_012">applications.</span></div>
<div style="position:absolute;left:62.08px;top:395.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  SQL relational standards: SQL-89 (SQL1), SQL-92 (SQL2),</span></div>
<div style="position:absolute;left:84.58px;top:421.52px" class="cls_012"><span class="cls_012">SQL-99, SQL3, …</span></div>
<div style="position:absolute;left:62.08px;top:453.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Chapters 5 through 11 describe this model in detail</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 51</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background52.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Object-oriented Data Models:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Several models have been proposed for implementing in a</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">database system.</span></div>
<div style="position:absolute;left:62.08px;top:221.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  One set comprises models of persistent O-O Programming</span></div>
<div style="position:absolute;left:84.58px;top:247.52px" class="cls_012"><span class="cls_012">Languages such as C++ (e.g., in OBJECTSTORE or</span></div>
<div style="position:absolute;left:84.58px;top:273.68px" class="cls_012"><span class="cls_012">VERSANT), and Smalltalk (e.g., in GEMSTONE).</span></div>
<div style="position:absolute;left:62.08px;top:305.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Additionally, systems like O2, ORION (at MCC - then</span></div>
<div style="position:absolute;left:84.58px;top:331.52px" class="cls_012"><span class="cls_012">ITASCA), IRIS (at H.P.- used in Open OODB).</span></div>
<div style="position:absolute;left:62.08px;top:363.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Object Database Standard: ODMG-93, ODMG-version 2.0,</span></div>
<div style="position:absolute;left:84.58px;top:390.56px" class="cls_012"><span class="cls_012">ODMG-version 3.0.</span></div>
<div style="position:absolute;left:62.08px;top:421.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Chapter 12 describes this model.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 52</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background53.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">History of Data Models</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_014">  Object-Relational Models:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The trend to mix object models with relational was</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_016"><span class="cls_016">started with Informix Universal Server.</span></div>
<div style="position:absolute;left:62.08px;top:237.52px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Relational systems incorporated concepts from</span></div>
<div style="position:absolute;left:84.58px;top:268.48px" class="cls_016"><span class="cls_016">object databases leading to object-relational.</span></div>
<div style="position:absolute;left:62.08px;top:305.68px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Exemplified in the versions of Oracle, DB2, and</span></div>
<div style="position:absolute;left:84.58px;top:337.60px" class="cls_016"><span class="cls_016">SQL Server and other DBMSs.</span></div>
<div style="position:absolute;left:62.08px;top:374.56px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  Current trend by Relational DBMS vendors is to</span></div>
<div style="position:absolute;left:84.58px;top:405.52px" class="cls_016"><span class="cls_016">extend relational DBMSs with capability to process</span></div>
<div style="position:absolute;left:84.58px;top:436.48px" class="cls_016"><span class="cls_016">XML, Text and other data types.</span></div>
<div style="position:absolute;left:62.08px;top:474.64px" class="cls_015"><span class="cls_015">n</span><span class="cls_016">  The term “Object-relational” is receding in the</span></div>
<div style="position:absolute;left:84.58px;top:505.60px" class="cls_016"><span class="cls_016">marketplace.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 53</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter2/background54.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Chapter Summary</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Models and Their Categories</span></div>
<div style="position:absolute;left:26.08px;top:162.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Schemas, Instances, and States</span></div>
<div style="position:absolute;left:26.08px;top:199.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Three-Schema Architecture</span></div>
<div style="position:absolute;left:26.08px;top:236.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data Independence</span></div>
<div style="position:absolute;left:26.08px;top:273.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  DBMS Languages and Interfaces</span></div>
<div style="position:absolute;left:26.08px;top:310.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database System Utilities and Tools</span></div>
<div style="position:absolute;left:26.08px;top:347.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Database System Environment</span></div>
<div style="position:absolute;left:26.08px;top:384.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Centralized and Client-Server Architectures</span></div>
<div style="position:absolute;left:26.08px;top:421.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Classification of DBMSs</span></div>
<div style="position:absolute;left:26.08px;top:458.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  History of Data Models</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 2- 54</span></div>
</div>

</body>
</html>
