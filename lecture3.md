## Quiz      

1. Entity 
    - independent existence 
    - distinguishable 
    - noun 
    - physical existence 

2. Attribute 
    - description of the entity 
    - Types of Attributes 
        - Simple attribute 
        - Multi-value attribute 
        - Composite attribute 
        - combination of Multi-value attribute and Composite attribute 
        - key attributes 
        - derived attribute 

3. Relationship 
    - verb
    - interaction among entity 
    - cardinality entity relationship 
        - min 
        - max 


