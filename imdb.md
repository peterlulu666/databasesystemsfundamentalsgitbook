<iframe 
height="842"
width="100%"
src="Fall-2021-CSE-5330-Homework-Deliverables-v2 (1).pdf"></iframe>

- Set 1a

```sql
select tb.STARTYEAR, tb.GENRES, (
	select count(*) 
	from sharmac.TITLE_BASICS tbo, sharmac.TITLE_RATINGS tro 
	where tbo.TCONST = tro.TCONST and 
	  (tbo.STARTYEAR = tb.STARTYEAR) and 
	  (tbo.GENRES like '%Comedy%') 
	  and 
	  tbo.TITLETYPE = 'movie' and 
	  tro.AVERAGERATING > all (
	  		select avg(tri.AVERAGERATING)
	  		from sharmac.TITLE_RATINGS tri, sharmac.TITLE_BASICS tbi 
	  		where tri.TCONST = tbi.TCONST and 
	  		(tbi.STARTYEAR = tbo.STARTYEAR) and 
	  		(tbi.GENRES like '%Comedy%') and 
	  		tbi.TITLETYPE = 'movie' 
	   )
) as Above_avg  
from sharmac.TITLE_BASICS tb 
where (tb.STARTYEAR = '2004' or
	  tb.STARTYEAR = '2005' or 
	  tb.STARTYEAR = '2006' or 
	  tb.STARTYEAR = '2007' or 
	  tb.STARTYEAR = '2008' or 
	  tb.STARTYEAR = '2009') and 
	  (tb.GENRES = 'Comedy') and 
	  tb.TITLETYPE = 'movie'
group by tb.GENRES, tb.STARTYEAR 
order by tb.GENRES, tb.STARTYEAR; 
```

- Set 1b

```sql
select tb.STARTYEAR, tb.GENRES, (
	select count(*) 
	from sharmac.TITLE_BASICS tbo
	-- https://stackoverflow.com/questions/206484/sql-switch-case-in-where-clause
	-- it shows the if else statement without a case statement 
	where (tbo.STARTYEAR = tb.STARTYEAR) and 
	   ((tb.GENRES = 'Comedy' and tbo.GENRES like '%Comedy%') 
	   or 
	   (tb.GENRES = 'Drama' and tbo.GENRES like '%Drama%') 
	   or 
	   (tb.GENRES = 'Horror' and tbo.GENRES like '%Horror%') 
	   or 
	   (tb.GENRES = 'Sci-Fi' and tbo.GENRES like '%Sci-Fi%')) 
	  and 
	  tbo.TITLETYPE = 'movie' 
) as Movies_produced 
from sharmac.TITLE_BASICS tb 
where (tb.STARTYEAR = '2004' or
	  tb.STARTYEAR = '2005' or 
	  tb.STARTYEAR = '2006' or 
	  tb.STARTYEAR = '2007' or 
	  tb.STARTYEAR = '2008' or 
	  tb.STARTYEAR = '2009') and 
	  (tb.GENRES = 'Comedy' or  
	  tb.GENRES = 'Drama' or 
	  tb.GENRES = 'Horror' or 
	  tb.GENRES = 'Sci-Fi') and 
	  tb.TITLETYPE = 'movie'
group by tb.GENRES, tb.STARTYEAR 
order by tb.GENRES, tb.STARTYEAR; 

select tb.STARTYEAR, tb.GENRES, (
	select count(*) 
	from sharmac.TITLE_BASICS tbo
	-- https://stackoverflow.com/questions/206484/sql-switch-case-in-where-clause 
	-- https://svenweller.wordpress.com/2018/04/05/basic-sql-if-then-else/
	-- it shows case statement 
	where (tbo.STARTYEAR = tb.STARTYEAR) and 
	   	  tbo.GENRES like 
	   			(case when tb.GENRES in ('Comedy')
	         		  then '%Comedy%' 
	         		  when tb.GENRES in ('Drama')
	         		  then '%Drama%'
	         		  when tb.GENRES in ('Horror')
	         		  then '%Horror%'
	         		  when tb.GENRES in ('Sci-Fi')
	         		  then '%Sci-Fi%'
	         		  else null 
	    		end) 
	  and 
	  tbo.TITLETYPE = 'movie' 
) as Movies_produced 
from sharmac.TITLE_BASICS tb 
where (tb.STARTYEAR = '2004' or
	  tb.STARTYEAR = '2005' or 
	  tb.STARTYEAR = '2006' or 
	  tb.STARTYEAR = '2007' or 
	  tb.STARTYEAR = '2008' or 
	  tb.STARTYEAR = '2009') and 
	  (tb.GENRES = 'Comedy' or  
	  tb.GENRES = 'Drama' or 
	  tb.GENRES = 'Horror' or 
	  tb.GENRES = 'Sci-Fi') and 
	  tb.TITLETYPE = 'movie'
group by tb.GENRES, tb.STARTYEAR 
order by tb.GENRES, tb.STARTYEAR; 
```

- Set 2a

<iframe 
height="842"
width="100%"
src="set2a.png"></iframe>

```sql
select tb.startyear, sum(tr.AVERAGERATING) / count(tr.AVERAGERATING) as YEARLY_AVG
from sharmac.TITLE_BASICS tb, sharmac.TITLE_RATINGS tr 
where tb.TCONST = tr.TCONST and 
	  (tb.GENRES like '%Drama%' or 
	  tb.GENRES like '%Horror%' or
	  tb.GENRES like '%Sci-Fi%' or
	  tb.GENRES like '%Thriller%') and 
	  tb.GENRES not like '%\n%' and 
	  (tb.startyear ='2010' or 
	  tb.startyear = '2011' or 
	  tb.startyear = '2012' or 
	  tb.startyear = '2013' or 
	  tb.startyear = '2014' or 
	  tb.startyear = '2015') and 
	  tr.AVERAGERATING not like '%\n%' and 
	  TITLETYPE = 'movie' 
group by tb.startyear 
order by tb.startyear; 
```

- Set 2b

<iframe 
height="842"
width="100%"
src="set2b.pdf"></iframe>

```sql
select STARTYEAR, GENRES, (
	select avg(tro.AVERAGERATING) 
	from sharmac.TITLE_BASICS tbo, sharmac.TITLE_RATINGS tro 
	-- https://stackoverflow.com/questions/206484/sql-switch-case-in-where-clause
	-- it shows the if else statement without a case statement 
	where tbo.TCONST = tro.TCONST and 
		  ((tb.GENRES = 'Drama' and tbo.GENRES like '%Drama%') 
	   	  or 
	      (tb.GENRES = 'Horror' and tbo.GENRES like '%Horror%') 
	      or 
	      (tb.GENRES = 'Sci-Fi' and tbo.GENRES like '%Sci-Fi%') 
	      or 
	      (tb.GENRES = 'Thriller' and tbo.GENRES like '%Thriller%')) 
	      and 
		  tbo.startyear = tb.startyear and 
		  tbo.TITLETYPE = 'movie' 
) as YEARLY_AVG 
from sharmac.TITLE_BASICS tb, sharmac.TITLE_RATINGS tr 
where tb.TCONST = tr.TCONST and 
	  (tb.startyear ='2010' or 
	  tb.startyear = '2011' or 
	  tb.startyear = '2012' or 
	  tb.startyear = '2013' or 
	  tb.startyear = '2014' or 
	  tb.startyear = '2015') and 
	  (tb.GENRES = 'Drama' or  
	  tb.GENRES = 'Horror' or 
	  tb.GENRES = 'Sci-Fi' or 
	  tb.GENRES = 'Thriller') and 
	  tb.GENRES not like '%\n%' and 
	  tr.AVERAGERATING not like '%\n%' and 
	  tb.TITLETYPE = 'movie'
group by STARTYEAR, GENRES 
order by STARTYEAR; 
```



































