<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:32.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:36.0px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:36.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:17.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:14.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:12.1px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:11.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:28.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:32.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:14.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:28.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:24.1px;color:rgb(51,51,153);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:26.0px;color:rgb(128,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_029{font-family:Arial,serif;font-size:13.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:Arial,serif;font-size:13.0px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:20.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:20.1px;color:rgb(0,175,80);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:10.0px;color:rgb(153,0,51);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_033{font-family:Arial,serif;font-size:20.1px;color:rgb(51,51,153);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:24.1px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:16.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:16.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:13.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:13.0px;color:rgb(128,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:15.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_037{font-family:Arial,serif;font-size:15.1px;color:rgb(51,51,153);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter1/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:258.33px;top:250.48px" class="cls_003"><span class="cls_003">CHAPTER 1</span></div>
<div style="position:absolute;left:82.01px;top:343.68px" class="cls_004"><span class="cls_004">Databases and Database Users</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">OUTLINE</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Types of Databases and Database Applications</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Basic Definitions</span></div>
<div style="position:absolute;left:26.08px;top:209.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Typical DBMS Functionality</span></div>
<div style="position:absolute;left:26.08px;top:249.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Example of a Database (UNIVERSITY)</span></div>
<div style="position:absolute;left:26.08px;top:290.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Main Characteristics of the Database Approach</span></div>
<div style="position:absolute;left:26.08px;top:330.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Types of Database Users</span></div>
<div style="position:absolute;left:26.08px;top:370.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Advantages of Using the Database Approach</span></div>
<div style="position:absolute;left:26.08px;top:410.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Historical Development of Database Technology</span></div>
<div style="position:absolute;left:26.08px;top:451.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Extending Database Capabilities</span></div>
<div style="position:absolute;left:26.08px;top:491.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  When Not to Use Databases</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Types of Databases and Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Applications</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Traditional Applications:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Numeric and Textual Databases</span></div>
<div style="position:absolute;left:26.08px;top:195.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  More Recent Applications:</span></div>
<div style="position:absolute;left:62.08px;top:229.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Multimedia Databases</span></div>
<div style="position:absolute;left:62.08px;top:260.48px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Geographic Information Systems (GIS)</span></div>
<div style="position:absolute;left:62.08px;top:292.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Biological and Genome Databases</span></div>
<div style="position:absolute;left:62.08px;top:324.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Data Warehouses</span></div>
<div style="position:absolute;left:62.08px;top:355.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Mobile databases</span></div>
<div style="position:absolute;left:62.08px;top:387.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Real-time and Active Databases</span></div>
<div style="position:absolute;left:26.08px;top:419.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  First part of book focuses on traditional applications</span></div>
<div style="position:absolute;left:26.08px;top:454.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_013">  A number of recent applications are described later in the</span></div>
<div style="position:absolute;left:53.08px;top:483.60px" class="cls_013"><span class="cls_013">book (for example, Chapters 24,25,26,27,28,29)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Recent Developments (1)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Social Networks started capturing a lot of</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">information about people and about</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_008"><span class="cls_008">communications among people-posts, tweets,</span></div>
<div style="position:absolute;left:53.08px;top:229.52px" class="cls_008"><span class="cls_008">photos, videos in systems such as:</span></div>
<div style="position:absolute;left:26.08px;top:269.60px" class="cls_008"><span class="cls_008">- Facebook</span></div>
<div style="position:absolute;left:26.08px;top:310.64px" class="cls_008"><span class="cls_008">- Twitter</span></div>
<div style="position:absolute;left:26.08px;top:350.48px" class="cls_008"><span class="cls_008">- Linked-In</span></div>
<div style="position:absolute;left:26.08px;top:390.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  All of the above constitutes data</span></div>
<div style="position:absolute;left:26.08px;top:431.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Search Engines- Google, Bing, Yahoo : collect</span></div>
<div style="position:absolute;left:53.08px;top:464.48px" class="cls_008"><span class="cls_008">their own repository of web pages for searching</span></div>
<div style="position:absolute;left:53.08px;top:498.56px" class="cls_008"><span class="cls_008">purposes</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Recent Developments (2)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  New Technologies are emerging from the so-</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">called non-database software vendors to manage</span></div>
<div style="position:absolute;left:53.08px;top:195.68px" class="cls_008"><span class="cls_008">vast amounts of data generated on the web:</span></div>
<div style="position:absolute;left:26.08px;top:269.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Big Data storage systems involving large clusters</span></div>
<div style="position:absolute;left:53.08px;top:303.68px" class="cls_008"><span class="cls_008">of distributed computers (Chapter 25)</span></div>
<div style="position:absolute;left:26.08px;top:343.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  NOSQL (Not Only SQL) systems (Chapter 24)</span></div>
<div style="position:absolute;left:26.08px;top:384.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  A large amount of data now resides on the</span></div>
<div style="position:absolute;left:53.08px;top:417.68px" class="cls_008"><span class="cls_008">“cloud” which means it is in huge data centers</span></div>
<div style="position:absolute;left:53.08px;top:451.52px" class="cls_008"><span class="cls_008">using thousands of machines.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Basic Definitions</span></div>
<div style="position:absolute;left:26.08px;top:126.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">   Database:</span></div>
<div style="position:absolute;left:62.08px;top:152.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   A collection of related data.</span></div>
<div style="position:absolute;left:26.08px;top:179.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">   Data:</span></div>
<div style="position:absolute;left:62.08px;top:205.60px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Known facts that can be recorded and have an implicit meaning.</span></div>
<div style="position:absolute;left:26.08px;top:232.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">   Mini-world:</span></div>
<div style="position:absolute;left:62.08px;top:258.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Some part of the real world about which data is stored in a</span></div>
<div style="position:absolute;left:84.58px;top:280.48px" class="cls_017"><span class="cls_017">database. For example, student grades and transcripts at a</span></div>
<div style="position:absolute;left:84.58px;top:301.60px" class="cls_017"><span class="cls_017">university.</span></div>
<div style="position:absolute;left:26.08px;top:328.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">   Database Management System (DBMS):</span></div>
<div style="position:absolute;left:62.08px;top:354.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   A software package/ system to facilitate the creation and</span></div>
<div style="position:absolute;left:84.58px;top:376.48px" class="cls_017"><span class="cls_017">maintenance of a computerized database.</span></div>
<div style="position:absolute;left:26.08px;top:402.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_015">   Database System:</span></div>
<div style="position:absolute;left:62.08px;top:428.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   The DBMS software together with the data itself.  Sometimes, the</span></div>
<div style="position:absolute;left:84.58px;top:450.64px" class="cls_017"><span class="cls_017">applications are also included.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Impact of Databases and Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Technology</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Businesses: </span><span class="cls_018">Banking, Insurance, Retail,</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_018"><span class="cls_018">Transportation, Healthcare, Manufacturing</span></div>
<div style="position:absolute;left:26.08px;top:202.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Service Industries: </span><span class="cls_018">Financial, Real-estate, Legal,</span></div>
<div style="position:absolute;left:53.08px;top:236.48px" class="cls_018"><span class="cls_018">Electronic Commerce, Small businesses</span></div>
<div style="position:absolute;left:26.08px;top:276.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Education : </span><span class="cls_018">Resources for content and Delivery</span></div>
<div style="position:absolute;left:26.08px;top:316.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  More recently: </span><span class="cls_018">Social Networks, Environmental</span></div>
<div style="position:absolute;left:53.08px;top:350.48px" class="cls_018"><span class="cls_018">and Scientific Applications, Medicine and</span></div>
<div style="position:absolute;left:53.08px;top:384.56px" class="cls_018"><span class="cls_018">Genetics</span></div>
<div style="position:absolute;left:26.08px;top:424.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Personalized Applications: </span><span class="cls_018">based on smart</span></div>
<div style="position:absolute;left:53.08px;top:458.48px" class="cls_018"><span class="cls_018">mobile devices</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_019"><span class="cls_019">Simplified database system environment</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:627.45px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Typical DBMS Functionality</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_013">  Define</span><span class="cls_010"> a particular database in terms of its data types,</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_010"><span class="cls_010">structures, and constraints</span></div>
<div style="position:absolute;left:26.08px;top:192.48px" class="cls_009"><span class="cls_009">n</span><span class="cls_013">  Construct</span><span class="cls_010"> or Load the initial database contents on a</span></div>
<div style="position:absolute;left:53.08px;top:221.52px" class="cls_010"><span class="cls_010">secondary storage medium</span></div>
<div style="position:absolute;left:26.08px;top:255.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_013">  Manipulating</span><span class="cls_010"> the database:</span></div>
<div style="position:absolute;left:62.08px;top:289.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Retrieval: Querying, generating reports</span></div>
<div style="position:absolute;left:62.08px;top:321.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Modification: Insertions, deletions and updates to its content</span></div>
<div style="position:absolute;left:62.08px;top:353.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Accessing the database through Web applications</span></div>
<div style="position:absolute;left:26.08px;top:385.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_013">  Processing</span><span class="cls_010"> and </span><span class="cls_013">Sharing</span><span class="cls_010"> by a set of concurrent users and</span></div>
<div style="position:absolute;left:53.08px;top:413.52px" class="cls_010"><span class="cls_010">application programs - yet, keeping all data valid and</span></div>
<div style="position:absolute;left:53.08px;top:442.56px" class="cls_010"><span class="cls_010">consistent</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Application Activities Against a</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Database</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Applications interact with a database by</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">generating</span></div>
<div style="position:absolute;left:26.08px;top:202.64px" class="cls_008"><span class="cls_008">- Queries: </span><span class="cls_018">that access different parts of data and</span></div>
<div style="position:absolute;left:53.08px;top:236.48px" class="cls_018"><span class="cls_018">formulate the result of a request</span></div>
<div style="position:absolute;left:26.08px;top:276.56px" class="cls_008"><span class="cls_008">- Transactions: </span><span class="cls_018">that may read some data and</span></div>
<div style="position:absolute;left:53.08px;top:310.64px" class="cls_018"><span class="cls_018">“update” certain values or generate new data and</span></div>
<div style="position:absolute;left:53.08px;top:343.52px" class="cls_018"><span class="cls_018">store that in the database</span></div>
<div style="position:absolute;left:26.08px;top:384.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Applications must not allow unauthorized users to</span></div>
<div style="position:absolute;left:53.08px;top:417.68px" class="cls_008"><span class="cls_008">access data</span></div>
<div style="position:absolute;left:26.08px;top:458.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Applications must keep up with changing user</span></div>
<div style="position:absolute;left:53.08px;top:491.60px" class="cls_008"><span class="cls_008">requirements against the database</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:620.47px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Additional DBMS Functionality</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  DBMS may additionally provide:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Protection or Security measures to prevent</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_021"><span class="cls_021">unauthorized access</span></div>
<div style="position:absolute;left:62.08px;top:237.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  “Active” processing to take internal actions on data</span></div>
<div style="position:absolute;left:62.08px;top:274.48px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Presentation and Visualization of data</span></div>
<div style="position:absolute;left:62.08px;top:312.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Maintenance of the database and associated</span></div>
<div style="position:absolute;left:84.58px;top:343.60px" class="cls_021"><span class="cls_021">programs over the lifetime of the database</span></div>
<div style="position:absolute;left:84.58px;top:374.56px" class="cls_021"><span class="cls_021">application</span></div>
<div style="position:absolute;left:98.08px;top:411.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_010"> Called database, software, and system</span></div>
<div style="position:absolute;left:116.07px;top:440.64px" class="cls_010"><span class="cls_010">maintenance</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Example of a Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">(with a Conceptual Data Model)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_022">  Mini-world for the example:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Part of a UNIVERSITY environment.</span></div>
<div style="position:absolute;left:26.08px;top:206.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_022">  Some mini-world </span><span class="cls_023">entities</span><span class="cls_022">:</span></div>
<div style="position:absolute;left:62.08px;top:246.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  STUDENTs</span></div>
<div style="position:absolute;left:62.08px;top:283.60px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  COURSEs</span></div>
<div style="position:absolute;left:62.08px;top:321.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  SECTIONs (of COURSEs)</span></div>
<div style="position:absolute;left:62.08px;top:358.48px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  (academic) DEPARTMENTs</span></div>
<div style="position:absolute;left:62.08px;top:396.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  INSTRUCTORs</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Example of a Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">(with a Conceptual Data Model)</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Some mini-world </span><span class="cls_025">relationships</span><span class="cls_024">:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  SECTIONs </span><span class="cls_026">are of specific</span><span class="cls_012"> COURSEs</span></div>
<div style="position:absolute;left:62.08px;top:194.48px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  STUDENTs </span><span class="cls_026">take</span><span class="cls_012"> SECTIONs</span></div>
<div style="position:absolute;left:62.08px;top:226.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  COURSEs </span><span class="cls_026">have  prerequisite</span><span class="cls_012"> COURSEs</span></div>
<div style="position:absolute;left:62.08px;top:258.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  INSTRUCTORs </span><span class="cls_026">teach</span><span class="cls_012"> SECTIONs</span></div>
<div style="position:absolute;left:62.08px;top:289.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  COURSEs </span><span class="cls_026">are offered by</span><span class="cls_012"> DEPARTMENTs</span></div>
<div style="position:absolute;left:62.08px;top:321.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  STUDENTs </span><span class="cls_026">major in</span><span class="cls_012"> DEPARTMENTs</span></div>
<div style="position:absolute;left:26.08px;top:388.56px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Note: The above entities and relationships are typically</span></div>
<div style="position:absolute;left:53.08px;top:416.64px" class="cls_010"><span class="cls_010">expressed in a conceptual data model, such as the</span></div>
<div style="position:absolute;left:53.08px;top:445.68px" class="cls_010"><span class="cls_010">ENTITY-RELATIONSHIP data model (see Chapters 3, 4)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Example of a simple database</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Main Characteristics of the Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Approach</span></div>
<div style="position:absolute;left:26.08px;top:109.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Self-describing nature of a database system:</span></div>
<div style="position:absolute;left:62.08px;top:143.84px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  A DBMS </span><span class="cls_027">catalog</span><span class="cls_012"> stores the description of a particular</span></div>
<div style="position:absolute;left:84.58px;top:170.72px" class="cls_012"><span class="cls_012">database (e.g. data structures, types, and constraints)</span></div>
<div style="position:absolute;left:62.08px;top:202.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  The description is called </span><span class="cls_027">meta-data*</span><span class="cls_012">.</span></div>
<div style="position:absolute;left:62.08px;top:233.84px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  This allows the DBMS software to work with different</span></div>
<div style="position:absolute;left:84.58px;top:260.72px" class="cls_012"><span class="cls_012">database applications.</span></div>
<div style="position:absolute;left:26.08px;top:292.80px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Insulation between programs and data:</span></div>
<div style="position:absolute;left:62.08px;top:326.72px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Called </span><span class="cls_027">program-data independence</span><span class="cls_012">.</span></div>
<div style="position:absolute;left:62.08px;top:358.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Allows changing data structures and storage organization</span></div>
<div style="position:absolute;left:84.58px;top:384.80px" class="cls_012"><span class="cls_012">without having to change the DBMS access programs.</span></div>
<div style="position:absolute;left:62.08px;top:416.72px" class="cls_012"><span class="cls_012">-----------------------------------------------------------------------------</span></div>
<div style="position:absolute;left:62.08px;top:447.68px" class="cls_012"><span class="cls_012">* Some newer systems such as a few NOSQL systems need</span></div>
<div style="position:absolute;left:62.08px;top:474.80px" class="cls_012"><span class="cls_012">no meta-data: they store the data definition within its structure</span></div>
<div style="position:absolute;left:62.08px;top:500.72px" class="cls_012"><span class="cls_012">making it self describing</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:58.96px" class="cls_019"><span class="cls_019">Example of a simplified database catalog</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Main Characteristics of the Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Approach (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_022">  Data Abstraction:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  A </span><span class="cls_028">data model</span><span class="cls_021"> is used to hide storage details and</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_021"><span class="cls_021">present the users with a conceptual view  of the</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_021"><span class="cls_021">database.</span></div>
<div style="position:absolute;left:62.08px;top:268.48px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Programs refer to the data model constructs rather</span></div>
<div style="position:absolute;left:84.58px;top:299.68px" class="cls_021"><span class="cls_021">than data storage details</span></div>
<div style="position:absolute;left:26.08px;top:337.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_022">  Support of multiple views of the data:</span></div>
<div style="position:absolute;left:62.08px;top:377.68px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Each user may see a different view of the</span></div>
<div style="position:absolute;left:84.58px;top:408.64px" class="cls_021"><span class="cls_021">database, which describes </span><span class="cls_028">only</span><span class="cls_021"> the data of</span></div>
<div style="position:absolute;left:84.58px;top:439.60px" class="cls_021"><span class="cls_021">interest to that user.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Main Characteristics of the Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Approach (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_024">  Sharing of data and multi-user transaction</span></div>
<div style="position:absolute;left:53.08px;top:157.68px" class="cls_024"><span class="cls_024">processing:</span></div>
<div style="position:absolute;left:62.08px;top:191.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Allowing a set of </span><span class="cls_027">concurrent users</span><span class="cls_012"> to retrieve from and to</span></div>
<div style="position:absolute;left:84.58px;top:218.48px" class="cls_012"><span class="cls_012">update the database.</span></div>
<div style="position:absolute;left:62.08px;top:249.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_026">  Concurrency control</span><span class="cls_012"> within the DBMS guarantees that each</span></div>
<div style="position:absolute;left:84.58px;top:276.56px" class="cls_027"><span class="cls_027">transaction</span><span class="cls_012"> is correctly executed or aborted</span></div>
<div style="position:absolute;left:62.08px;top:307.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_026">  Recovery</span><span class="cls_012"> subsystem ensures each completed transaction</span></div>
<div style="position:absolute;left:84.58px;top:334.64px" class="cls_012"><span class="cls_012">has its effect permanently recorded in the database</span></div>
<div style="position:absolute;left:62.08px;top:366.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_027">  OLTP</span><span class="cls_012"> (Online Transaction Processing) is a major part of</span></div>
<div style="position:absolute;left:84.58px;top:392.48px" class="cls_012"><span class="cls_012">database applications. This allows hundreds of concurrent</span></div>
<div style="position:absolute;left:84.58px;top:418.64px" class="cls_012"><span class="cls_012">transactions to execute per second.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Database Users</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Users may be divided into</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Those who actually use and control the database</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_021"><span class="cls_021">content, and those who design, develop and</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_021"><span class="cls_021">maintain database applications (called “Actors on</span></div>
<div style="position:absolute;left:84.58px;top:262.48px" class="cls_021"><span class="cls_021">the Scene”), and</span></div>
<div style="position:absolute;left:62.08px;top:299.68px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Those who design and develop the DBMS</span></div>
<div style="position:absolute;left:84.58px;top:330.64px" class="cls_021"><span class="cls_021">software and related tools, and the computer</span></div>
<div style="position:absolute;left:84.58px;top:362.56px" class="cls_021"><span class="cls_021">systems operators (called “Workers Behind the</span></div>
<div style="position:absolute;left:84.58px;top:393.52px" class="cls_021"><span class="cls_021">Scene”).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:8.16px" class="cls_006"><span class="cls_006">Database Users - Actors on the</span></div>
<div style="position:absolute;left:25.20px;top:51.36px" class="cls_006"><span class="cls_006">Scene</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Actors on the scene</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_028">  Database administrators:</span></div>
<div style="position:absolute;left:98.08px;top:205.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_010"> Responsible for authorizing access to the database,</span></div>
<div style="position:absolute;left:116.07px;top:234.48px" class="cls_010"><span class="cls_010">for coordinating and monitoring its use, acquiring</span></div>
<div style="position:absolute;left:116.07px;top:263.52px" class="cls_010"><span class="cls_010">software and hardware resources, controlling its use</span></div>
<div style="position:absolute;left:116.07px;top:292.56px" class="cls_010"><span class="cls_010">and monitoring efficiency of operations.</span></div>
<div style="position:absolute;left:62.08px;top:327.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_028">  Database Designers:</span></div>
<div style="position:absolute;left:98.08px;top:364.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_010"> Responsible to define the content, the structure, the</span></div>
<div style="position:absolute;left:116.07px;top:392.64px" class="cls_010"><span class="cls_010">constraints, and functions or transactions against</span></div>
<div style="position:absolute;left:116.07px;top:421.68px" class="cls_010"><span class="cls_010">the database. They must communicate with the</span></div>
<div style="position:absolute;left:116.07px;top:450.48px" class="cls_010"><span class="cls_010">end-users and understand their needs.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Database End Users</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Actors on the scene (continued)</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_028">  End-users: </span><span class="cls_021">They use the data for queries, reports</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_021"><span class="cls_021">and some of them update the database content.</span></div>
<div style="position:absolute;left:84.58px;top:218.56px" class="cls_021"><span class="cls_021">End-users can be categorized into:</span></div>
<div style="position:absolute;left:98.08px;top:252.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Casual</span><span class="cls_010">: access database occasionally when</span></div>
<div style="position:absolute;left:116.07px;top:278.64px" class="cls_010"><span class="cls_010">needed</span></div>
<div style="position:absolute;left:98.08px;top:310.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Naïve</span><span class="cls_010"> or Parametric: they make up a large section</span></div>
<div style="position:absolute;left:116.07px;top:335.52px" class="cls_010"><span class="cls_010">of the end-user population.</span></div>
<div style="position:absolute;left:134.07px;top:367.60px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  They use previously well-defined functions in the form of</span></div>
<div style="position:absolute;left:152.07px;top:388.48px" class="cls_017"><span class="cls_017">“canned transactions” against the database.</span></div>
<div style="position:absolute;left:134.07px;top:415.60px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Users of Mobile Apps mostly fall in this category</span></div>
<div style="position:absolute;left:134.07px;top:441.52px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Bank-tellers or reservation clerks are parametric users</span></div>
<div style="position:absolute;left:152.07px;top:463.60px" class="cls_017"><span class="cls_017">who do this activity for an entire shift of operations.</span></div>
<div style="position:absolute;left:134.07px;top:489.52px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Social Media Users post and read information from</span></div>
<div style="position:absolute;left:152.07px;top:525.24px" class="cls_017"><span class="cls_017">websites</span><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Database End Users (continued)</span></div>
<div style="position:absolute;left:98.08px;top:128.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Sophisticated:</span></div>
<div style="position:absolute;left:134.07px;top:162.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  These include business analysts, scientists, engineers,</span></div>
<div style="position:absolute;left:152.07px;top:186.64px" class="cls_017"><span class="cls_017">others thoroughly familiar with the system capabilities.</span></div>
<div style="position:absolute;left:134.07px;top:215.68px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Many use tools in the form of software packages that work</span></div>
<div style="position:absolute;left:152.07px;top:239.68px" class="cls_017"><span class="cls_017">closely with the stored database.</span></div>
<div style="position:absolute;left:98.08px;top:269.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Stand-alone:</span></div>
<div style="position:absolute;left:134.07px;top:302.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Mostly maintain personal databases using ready-to-use</span></div>
<div style="position:absolute;left:152.07px;top:326.56px" class="cls_017"><span class="cls_017">packaged applications.</span></div>
<div style="position:absolute;left:134.07px;top:355.60px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  An example is the user of a tax program that creates its</span></div>
<div style="position:absolute;left:152.07px;top:379.60px" class="cls_017"><span class="cls_017">own internal database.</span></div>
<div style="position:absolute;left:134.07px;top:408.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">  Another example is a user that maintains a database of</span></div>
<div style="position:absolute;left:152.07px;top:432.64px" class="cls_017"><span class="cls_017">personal photos and videos.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Database Users - Actors on the</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Scene (continued)</span></div>
<div style="position:absolute;left:97.20px;top:125.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> System Analysts and Application Developers</span></div>
<div style="position:absolute;left:136.95px;top:164.56px" class="cls_017"><span class="cls_017">This category currently accounts for a very large proportion</span></div>
<div style="position:absolute;left:115.20px;top:189.52px" class="cls_017"><span class="cls_017">of the IT work force.</span></div>
<div style="position:absolute;left:133.20px;top:218.64px" class="cls_029"><span class="cls_029">n</span><span class="cls_024"> System Analysts</span><span class="cls_017">: They understand the user</span></div>
<div style="position:absolute;left:151.20px;top:247.60px" class="cls_017"><span class="cls_017">requirements of naïve and sophisticated users and design</span></div>
<div style="position:absolute;left:151.20px;top:271.60px" class="cls_017"><span class="cls_017">applications including canned  transactions to meet those</span></div>
<div style="position:absolute;left:151.20px;top:295.60px" class="cls_017"><span class="cls_017">requirements.</span></div>
<div style="position:absolute;left:133.20px;top:325.68px" class="cls_029"><span class="cls_029">n</span><span class="cls_024"> Application Programmers: </span><span class="cls_017">Implement the</span></div>
<div style="position:absolute;left:151.20px;top:354.64px" class="cls_017"><span class="cls_017">specifications developed by analysts and test and debug</span></div>
<div style="position:absolute;left:151.20px;top:378.64px" class="cls_017"><span class="cls_017">them before deployment.</span></div>
<div style="position:absolute;left:133.20px;top:408.48px" class="cls_029"><span class="cls_029">n</span><span class="cls_024"> Business Analysts</span><span class="cls_017">: There is an increasing need for</span></div>
<div style="position:absolute;left:151.20px;top:436.48px" class="cls_017"><span class="cls_017">such people who can analyze vast amounts of business</span></div>
<div style="position:absolute;left:151.20px;top:460.48px" class="cls_017"><span class="cls_017">data and real-time data (“Big Data”) for better decision</span></div>
<div style="position:absolute;left:151.20px;top:484.48px" class="cls_017"><span class="cls_017">making related to planning, advertising, marketing etc.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Database Users - Workers behind</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">the Scene</span></div>
<div style="position:absolute;left:97.20px;top:125.52px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> System Designers and Implementors: </span><span class="cls_017">Design and</span></div>
<div style="position:absolute;left:115.20px;top:154.48px" class="cls_017"><span class="cls_017">implement DBMS packages in the form of </span><span class="cls_030">modules</span><span class="cls_017"> and</span></div>
<div style="position:absolute;left:115.20px;top:178.48px" class="cls_030"><span class="cls_030">interfaces</span><span class="cls_017"> and test and debug them. The DBMS must interface</span></div>
<div style="position:absolute;left:115.20px;top:202.48px" class="cls_017"><span class="cls_017">with applications, language compilers, operating system</span></div>
<div style="position:absolute;left:115.20px;top:226.48px" class="cls_017"><span class="cls_017">components, etc.</span></div>
<div style="position:absolute;left:97.20px;top:256.56px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Tool Developers</span><span class="cls_010">: </span><span class="cls_017">Design and implement software</span></div>
<div style="position:absolute;left:115.20px;top:285.52px" class="cls_017"><span class="cls_017">systems called  tools for modeling and designing databases,</span></div>
<div style="position:absolute;left:115.20px;top:309.52px" class="cls_017"><span class="cls_017">performance monitoring, prototyping, test data generation,</span></div>
<div style="position:absolute;left:115.20px;top:333.52px" class="cls_017"><span class="cls_017">user interface creation, simulation etc. that facilitate building of</span></div>
<div style="position:absolute;left:115.20px;top:357.60px" class="cls_017"><span class="cls_017">applications and allow using database effectively</span><span class="cls_010">.</span></div>
<div style="position:absolute;left:97.20px;top:392.48px" class="cls_014"><span class="cls_014">n</span><span class="cls_024"> Operators and Maintenance Personnel</span><span class="cls_022">: </span><span class="cls_017">They</span></div>
<div style="position:absolute;left:115.20px;top:426.64px" class="cls_017"><span class="cls_017">manage the actual </span><span class="cls_030">running and maintenance </span><span class="cls_017">of the </span><span class="cls_030">database</span></div>
<div style="position:absolute;left:115.20px;top:450.64px" class="cls_030"><span class="cls_030">system hardware and software environment</span><span class="cls_017">.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Advantages of Using the DBMS</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Approach</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Controlling redundancy in data storage and in</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">development and maintenance efforts.</span></div>
<div style="position:absolute;left:62.08px;top:202.48px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Sharing of data among multiple users.</span></div>
<div style="position:absolute;left:26.08px;top:240.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Restricting unauthorized access to data. Only the</span></div>
<div style="position:absolute;left:53.08px;top:273.68px" class="cls_008"><span class="cls_008">DBA staff uses privileged commands and</span></div>
<div style="position:absolute;left:53.08px;top:307.52px" class="cls_008"><span class="cls_008">facilities.</span></div>
<div style="position:absolute;left:26.08px;top:347.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Providing persistent storage for program Objects</span></div>
<div style="position:absolute;left:62.08px;top:387.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  E.g., Object-oriented DBMSs make program</span></div>
<div style="position:absolute;left:84.58px;top:418.48px" class="cls_021"><span class="cls_021">objects persistent- see Chapter 12.</span></div>
<div style="position:absolute;left:26.08px;top:456.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Providing Storage Structures (e.g. indexes) for</span></div>
<div style="position:absolute;left:53.08px;top:490.64px" class="cls_008"><span class="cls_008">efficient Query Processing - see Chapter 17.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Advantages of Using the DBMS</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Approach (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Providing optimization of queries for efficient</span></div>
<div style="position:absolute;left:53.08px;top:162.56px" class="cls_008"><span class="cls_008">processing.</span></div>
<div style="position:absolute;left:26.08px;top:202.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Providing backup and recovery services.</span></div>
<div style="position:absolute;left:26.08px;top:242.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Providing multiple interfaces to different classes</span></div>
<div style="position:absolute;left:53.08px;top:276.56px" class="cls_008"><span class="cls_008">of users.</span></div>
<div style="position:absolute;left:26.08px;top:316.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Representing complex relationships among data.</span></div>
<div style="position:absolute;left:26.08px;top:357.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Enforcing integrity constraints on the database.</span></div>
<div style="position:absolute;left:26.08px;top:397.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Drawing inferences and actions from the stored</span></div>
<div style="position:absolute;left:53.08px;top:431.60px" class="cls_008"><span class="cls_008">data using deductive and active rules and</span></div>
<div style="position:absolute;left:53.08px;top:464.48px" class="cls_008"><span class="cls_008">triggers.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Additional Implications of Using the</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Database Approach</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Potential for enforcing standards:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  This is very crucial for the success of database</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_021"><span class="cls_021">applications in large organizations. </span><span class="cls_028">Standards</span></div>
<div style="position:absolute;left:84.58px;top:231.52px" class="cls_021"><span class="cls_021">refer to data item names, display formats, screens,</span></div>
<div style="position:absolute;left:84.58px;top:262.48px" class="cls_021"><span class="cls_021">report structures, meta-data (description of data),</span></div>
<div style="position:absolute;left:84.58px;top:293.68px" class="cls_021"><span class="cls_021">Web page layouts, etc.</span></div>
<div style="position:absolute;left:26.08px;top:331.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Reduced application development time:</span></div>
<div style="position:absolute;left:62.08px;top:371.68px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Incremental time to add each new application is</span></div>
<div style="position:absolute;left:84.58px;top:402.64px" class="cls_021"><span class="cls_021">reduced.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Additional Implications of Using the</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Database Approach (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Flexibility to change data structures:</span></div>
<div style="position:absolute;left:62.08px;top:168.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Database structure may evolve as new</span></div>
<div style="position:absolute;left:84.58px;top:199.60px" class="cls_021"><span class="cls_021">requirements are defined.</span></div>
<div style="position:absolute;left:26.08px;top:237.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Availability of current information:</span></div>
<div style="position:absolute;left:62.08px;top:277.60px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Extremely important for on-line transaction</span></div>
<div style="position:absolute;left:84.58px;top:308.56px" class="cls_021"><span class="cls_021">systems such as shopping, airline, hotel, car</span></div>
<div style="position:absolute;left:84.58px;top:339.52px" class="cls_021"><span class="cls_021">reservations.</span></div>
<div style="position:absolute;left:26.08px;top:377.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Economies of scale:</span></div>
<div style="position:absolute;left:62.08px;top:417.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Wasteful overlap of resources and personnel can</span></div>
<div style="position:absolute;left:84.58px;top:448.48px" class="cls_021"><span class="cls_021">be avoided by consolidating data and applications</span></div>
<div style="position:absolute;left:84.58px;top:480.64px" class="cls_021"><span class="cls_021">across departments.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Historical Development of Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Technology</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Early Database Applications:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  The Hierarchical and Network Models were introduced in</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">mid 1960s and dominated during the seventies.</span></div>
<div style="position:absolute;left:62.08px;top:221.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  A bulk of the worldwide database processing still occurs</span></div>
<div style="position:absolute;left:84.58px;top:247.52px" class="cls_012"><span class="cls_012">using these models, particularly, the hierarchical model</span></div>
<div style="position:absolute;left:84.58px;top:273.68px" class="cls_012"><span class="cls_012">using IBM’s IMS system.</span></div>
<div style="position:absolute;left:26.08px;top:305.52px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Relational Model based Systems:</span></div>
<div style="position:absolute;left:62.08px;top:340.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Relational model was originally introduced in 1970, was</span></div>
<div style="position:absolute;left:84.58px;top:366.56px" class="cls_012"><span class="cls_012">heavily researched and experimented within IBM Research</span></div>
<div style="position:absolute;left:84.58px;top:392.48px" class="cls_012"><span class="cls_012">and several universities.</span></div>
<div style="position:absolute;left:62.08px;top:424.64px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Relational DBMS Products emerged in the early 1980s.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Historical Development of Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Technology (continued)</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Object-oriented and emerging applications:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Object-Oriented Database Management Systems</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">(OODBMSs) were introduced in late 1980s and early 1990s</span></div>
<div style="position:absolute;left:84.58px;top:215.60px" class="cls_012"><span class="cls_012">to cater to the need of complex data processing in CAD and</span></div>
<div style="position:absolute;left:84.58px;top:242.48px" class="cls_012"><span class="cls_012">other applications.</span></div>
<div style="position:absolute;left:98.08px;top:273.52px" class="cls_031"><span class="cls_031">n</span><span class="cls_032">  Their use has not taken off much.</span></div>
<div style="position:absolute;left:62.08px;top:302.48px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Many relational DBMSs have incorporated object database</span></div>
<div style="position:absolute;left:84.58px;top:329.60px" class="cls_012"><span class="cls_012">concepts, leading to a new category called </span><span class="cls_026">object-relationa</span><span class="cls_012">l</span></div>
<div style="position:absolute;left:84.58px;top:355.52px" class="cls_012"><span class="cls_012">DBMSs (ORDBMSs)</span></div>
<div style="position:absolute;left:62.08px;top:387.68px" class="cls_011"><span class="cls_011">n</span><span class="cls_026">  Extended relational</span><span class="cls_012"> systems add further capabilities (e.g. for</span></div>
<div style="position:absolute;left:84.58px;top:413.60px" class="cls_012"><span class="cls_012">multimedia data, text, XML, and other data types)</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:11.04px" class="cls_006"><span class="cls_006">Historical Development of Database</span></div>
<div style="position:absolute;left:25.20px;top:54.00px" class="cls_006"><span class="cls_006">Technology (continued)</span></div>
<div style="position:absolute;left:26.08px;top:125.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Data on the Web and E-commerce Applications:</span></div>
<div style="position:absolute;left:62.08px;top:162.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Web contains data in HTML (Hypertext markup</span></div>
<div style="position:absolute;left:84.58px;top:190.48px" class="cls_021"><span class="cls_021">language) with links among pages.</span></div>
<div style="position:absolute;left:62.08px;top:224.56px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  This has given rise to a new set of applications</span></div>
<div style="position:absolute;left:84.58px;top:252.64px" class="cls_021"><span class="cls_021">and E-commerce is using new standards like XML</span></div>
<div style="position:absolute;left:84.58px;top:280.48px" class="cls_021"><span class="cls_021">(eXtended  Markup Language). (see Ch. 13).</span></div>
<div style="position:absolute;left:62.08px;top:315.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  Script programming languages such as PHP and</span></div>
<div style="position:absolute;left:84.58px;top:343.60px" class="cls_021"><span class="cls_021">JavaScript allow generation of dynamic Web</span></div>
<div style="position:absolute;left:84.58px;top:371.68px" class="cls_021"><span class="cls_021">pages that are partially generated from a database</span></div>
<div style="position:absolute;left:84.58px;top:399.52px" class="cls_021"><span class="cls_021">(see Ch. 11).</span></div>
<div style="position:absolute;left:98.08px;top:433.68px" class="cls_014"><span class="cls_014">n</span><span class="cls_010"> Also allow database updates through Web pages</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Extending Database Capabilities (1)</span></div>
<div style="position:absolute;left:26.08px;top:126.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_032">   New functionality is being added to DBMSs in the following areas:</span></div>
<div style="position:absolute;left:62.08px;top:152.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Scientific Applications - Physics, Chemistry, Biology - Genetics</span></div>
<div style="position:absolute;left:62.08px;top:179.68px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Earth and Atmospheric Sciences and Astronomy</span></div>
<div style="position:absolute;left:62.08px;top:205.60px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   XML (eXtensible Markup Language)</span></div>
<div style="position:absolute;left:62.08px;top:232.48px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Image Storage and Management</span></div>
<div style="position:absolute;left:62.08px;top:258.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Audio and Video Data Management</span></div>
<div style="position:absolute;left:62.08px;top:284.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Data Warehousing and Data Mining - a very major area for future</span></div>
<div style="position:absolute;left:84.58px;top:306.64px" class="cls_017"><span class="cls_017">development using new technologies (see Chapters 28-29)</span></div>
<div style="position:absolute;left:62.08px;top:332.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Spatial Data Management and Location Based Services</span></div>
<div style="position:absolute;left:62.08px;top:359.68px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Time Series and Historical Data Management</span></div>
<div style="position:absolute;left:26.08px;top:385.60px" class="cls_014"><span class="cls_014">n</span><span class="cls_032">   The above gives rise to </span><span class="cls_033">new research and development</span><span class="cls_032"> in</span></div>
<div style="position:absolute;left:53.08px;top:407.68px" class="cls_032"><span class="cls_032">incorporating new data types, complex data structures, new</span></div>
<div style="position:absolute;left:53.08px;top:428.56px" class="cls_032"><span class="cls_032">operations and storage and indexing schemes in database systems.</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Extending Database Capabilities (2)</span></div>
<div style="position:absolute;left:26.08px;top:126.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_032">   Background since the advent of the  21</span><span class="cls_029"><sup>st</sup></span><span class="cls_032"> Century:</span></div>
<div style="position:absolute;left:62.08px;top:179.52px" class="cls_029"><span class="cls_029">n</span><span class="cls_034">  First decade of the 21</span><span class="cls_035"><sup>st</sup></span><span class="cls_034"> century has seen tremendous</span></div>
<div style="position:absolute;left:84.58px;top:205.68px" class="cls_034"><span class="cls_034">growth in user generated data and automatically</span></div>
<div style="position:absolute;left:84.58px;top:231.60px" class="cls_034"><span class="cls_034">collected data from applications and search engines.</span></div>
<div style="position:absolute;left:62.08px;top:294.48px" class="cls_029"><span class="cls_029">n</span><span class="cls_034">  Social Media platforms such as Facebook and Twitter</span></div>
<div style="position:absolute;left:84.58px;top:320.64px" class="cls_034"><span class="cls_034">are generating millions of transactions a day and</span></div>
<div style="position:absolute;left:84.58px;top:346.56px" class="cls_034"><span class="cls_034">businesses are interested to tap into this data to</span></div>
<div style="position:absolute;left:84.58px;top:372.48px" class="cls_034"><span class="cls_034">“understand” the users</span></div>
<div style="position:absolute;left:62.08px;top:436.56px" class="cls_029"><span class="cls_029">n</span><span class="cls_034">  Cloud Storage and Backup is making unlimited amount</span></div>
<div style="position:absolute;left:84.58px;top:461.52px" class="cls_034"><span class="cls_034">of storage available to users and applications</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Extending Database Capabilities (3)</span></div>
<div style="position:absolute;left:26.08px;top:126.64px" class="cls_014"><span class="cls_014">n</span><span class="cls_032">   Emergence of Big Data Technologies and NOSQL databases</span></div>
<div style="position:absolute;left:62.08px;top:152.56px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   New data storage, management and analysis technology was</span></div>
<div style="position:absolute;left:84.58px;top:174.64px" class="cls_017"><span class="cls_017">necessary to deal with the onslaught of data in petabytes a day</span></div>
<div style="position:absolute;left:84.58px;top:196.48px" class="cls_017"><span class="cls_017">(10</span><span class="cls_036"><sup>15 </sup></span><span class="cls_017">bytes or 1000 terabytes) in some applications - this started</span></div>
<div style="position:absolute;left:84.58px;top:217.60px" class="cls_017"><span class="cls_017">being commonly called as “Big Data”.</span></div>
<div style="position:absolute;left:62.08px;top:244.48px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   Hadoop (which originated from Yahoo) and Mapreduce</span></div>
<div style="position:absolute;left:84.58px;top:265.60px" class="cls_017"><span class="cls_017">Programming approach to distributed data processing (which</span></div>
<div style="position:absolute;left:84.58px;top:287.68px" class="cls_017"><span class="cls_017">originated from Google) as well as the Google file system have</span></div>
<div style="position:absolute;left:84.58px;top:308.56px" class="cls_017"><span class="cls_017">given rise to Big Data technologies (Chapter 25). Further</span></div>
<div style="position:absolute;left:84.58px;top:330.64px" class="cls_017"><span class="cls_017">enhancements are taking place in the form of Spark based</span></div>
<div style="position:absolute;left:84.58px;top:352.48px" class="cls_017"><span class="cls_017">technology.</span></div>
<div style="position:absolute;left:62.08px;top:378.64px" class="cls_016"><span class="cls_016">n</span><span class="cls_017">   NOSQL (Not Only SQL- where SQL is the de facto standard</span></div>
<div style="position:absolute;left:84.58px;top:400.48px" class="cls_017"><span class="cls_017">language for relational DBMSs) systems have been designed for</span></div>
<div style="position:absolute;left:84.58px;top:421.60px" class="cls_017"><span class="cls_017">rapid search and retrieval from documents, processing of huge</span></div>
<div style="position:absolute;left:84.58px;top:443.68px" class="cls_017"><span class="cls_017">graphs occurring on social networks, and other forms of</span></div>
<div style="position:absolute;left:84.58px;top:464.56px" class="cls_017"><span class="cls_017">unstructured data with flexible models of transaction processing</span></div>
<div style="position:absolute;left:84.58px;top:486.64px" class="cls_017"><span class="cls_017">(Chapter 24).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:35.20px;top:54.24px" class="cls_006"><span class="cls_006">When not to use a DBMS</span></div>
<div style="position:absolute;left:26.08px;top:128.64px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  Main inhibitors (costs) of using a DBMS:</span></div>
<div style="position:absolute;left:62.08px;top:162.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  High initial investment and possible need for additional</span></div>
<div style="position:absolute;left:84.58px;top:189.68px" class="cls_012"><span class="cls_012">hardware.</span></div>
<div style="position:absolute;left:62.08px;top:221.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  Overhead for providing generality, security, concurrency</span></div>
<div style="position:absolute;left:84.58px;top:247.52px" class="cls_012"><span class="cls_012">control, recovery, and integrity functions.</span></div>
<div style="position:absolute;left:26.08px;top:279.60px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  When a DBMS may be unnecessary:</span></div>
<div style="position:absolute;left:62.08px;top:313.52px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  If the database and applications are simple, well defined,</span></div>
<div style="position:absolute;left:84.58px;top:340.64px" class="cls_012"><span class="cls_012">and not expected to change.</span></div>
<div style="position:absolute;left:62.08px;top:371.60px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  If access to data by multiple users is not required.</span></div>
<div style="position:absolute;left:26.08px;top:403.68px" class="cls_009"><span class="cls_009">n</span><span class="cls_010">  When a DBMS may be infeasible:</span></div>
<div style="position:absolute;left:62.08px;top:438.56px" class="cls_011"><span class="cls_011">n</span><span class="cls_012">  In embedded systems where a general purpose DBMS may</span></div>
<div style="position:absolute;left:84.58px;top:464.48px" class="cls_012"><span class="cls_012">not fit in available storage</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:35.20px;top:54.24px" class="cls_006"><span class="cls_006">When not to use a DBMS</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  When no DBMS may suffice:</span></div>
<div style="position:absolute;left:62.08px;top:169.52px" class="cls_037"><span class="cls_037">n</span><span class="cls_018"> If there are stringent real-time requirements</span></div>
<div style="position:absolute;left:84.58px;top:202.64px" class="cls_018"><span class="cls_018">that may not be met because of DBMS</span></div>
<div style="position:absolute;left:84.58px;top:236.48px" class="cls_018"><span class="cls_018">overhead (e.g., telephone switching systems)</span></div>
<div style="position:absolute;left:62.08px;top:276.64px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  If the database system is not able to handle the</span></div>
<div style="position:absolute;left:84.58px;top:307.60px" class="cls_021"><span class="cls_021">complexity of data because of modeling limitations</span></div>
<div style="position:absolute;left:84.58px;top:338.56px" class="cls_021"><span class="cls_021">(e.g., in complex genome and protein databases)</span></div>
<div style="position:absolute;left:62.08px;top:375.52px" class="cls_020"><span class="cls_020">n</span><span class="cls_021">  If the database users need special operations not</span></div>
<div style="position:absolute;left:84.58px;top:407.68px" class="cls_021"><span class="cls_021">supported by the DBMS (e.g., GIS and location</span></div>
<div style="position:absolute;left:84.58px;top:438.64px" class="cls_021"><span class="cls_021">based services).</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter1/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:25.20px;top:54.24px" class="cls_006"><span class="cls_006">Chapter Summary</span></div>
<div style="position:absolute;left:26.08px;top:128.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Types of Databases and Database Applications</span></div>
<div style="position:absolute;left:26.08px;top:169.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Basic Definitions</span></div>
<div style="position:absolute;left:26.08px;top:209.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Typical DBMS Functionality</span></div>
<div style="position:absolute;left:26.08px;top:249.68px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Example of a Database (UNIVERSITY)</span></div>
<div style="position:absolute;left:26.08px;top:290.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Main Characteristics of the Database Approach</span></div>
<div style="position:absolute;left:26.08px;top:330.56px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Types of Database Users</span></div>
<div style="position:absolute;left:26.08px;top:370.64px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Advantages of Using the Database Approach</span></div>
<div style="position:absolute;left:26.08px;top:410.48px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Historical Development of Database Technology</span></div>
<div style="position:absolute;left:26.08px;top:451.52px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  Extending Database Capabilities</span></div>
<div style="position:absolute;left:26.08px;top:491.60px" class="cls_007"><span class="cls_007">n</span><span class="cls_008">  When Not to Use Databases</span></div>
<div style="position:absolute;left:73.20px;top:525.24px" class="cls_002"><span class="cls_002">Copyright © 2016 Ramez Elmasri and Shamkant B. Navathe</span></div>
<div style="position:absolute;left:619.70px;top:519.52px" class="cls_005"><span class="cls_005">Slide 1- 38</span></div>
</div>

</body>
</html>
